﻿function OnAJAXError(e)
{
    var json = $.parseJSON(e.responseText);
    alert(json.errorMessage);
}

function FormDialog(dialogName, url, caption) {
    var mydialog = $("#" + dialogName);
    mydialog.load(url,
    function () {
        var form = mydialog.find("form");
        if (form.length > 0) {
            form.validate();
        }

        mydialog.dialog('option', 'buttons', { "确定": function () { form.submit();}, "关闭": function () { $(this).dialog("close"); } });
        mydialog.dialog({ title: caption });
        mydialog.dialog("open");
    });
}

function OpenDialog(dialogName, url, caption, issubmit, customBtn, gridName) {
    var mydialog = $("#" + dialogName);
    mydialog.load(url,
    function () {
        var form = mydialog.find("form");
        if (form.length > 0) {
            form.validate();
        }
        var buttons = "";
        if (issubmit) {
            buttons += "\"确定\": function () {dialogSubmit('" + dialogName + "','" + gridName + "');},";
        }
        if (customBtn != null)
        {
            buttons += customBtn+",";
        }
        buttons += " \"关闭\": function () { $(this).dialog(\"close\"); } ";
        eval("mydialog.dialog('option', 'buttons', {" + buttons + " });");
        //mydialog.dialog('option', 'buttons', { "关闭": function () { $(this).dialog("close"); } });
        mydialog.dialog({ title: caption });
        mydialog.dialog("open");
    });
}

function OpenAndInitDialog(dialogName, url, caption, loadCallBack, customBtn) {
    var mydialog = $("#" + dialogName);
    mydialog.load(url,
    function () {
        // 验证
        var form = mydialog.find("form");
        if (form.length > 0) {
            form.validate();
        }
        mydialog.dialog('option', 'buttons', { "确定": customBtn ,"关闭":function () { $(this).dialog("close");}});
        //mydialog.dialog('option', 'buttons', { "关闭": function () { $(this).dialog("close"); } });
        mydialog.dialog({ title: caption });
        mydialog.dialog("open");
        if (loadCallBack != null) {
            loadCallBack();
        }
    });
}

function dialogSubmit(name, gridName)
{
    var dialog = $("#" + name);
    var form = dialog.find("form");
    if (!form.valid()) {
        return;
    }

    $.ajax({
        url: form.attr('action'),
        type: "Post",
        data: form.serialize(),
        dataType: "json",
        success: function (data) {
            if (data.msg == true) {
                dialog.dialog("close");
                alert("操作成功！");
                if (gridName != null) {
                    jQuery("#" + gridName).trigger("reloadGrid");
                }
            }
            else {
                alert(data.errorMsg);
            }

        },
        error: function () {
            alert("错误，请重试！");
        }
    });
}

function AjaxPostData(url, mydata, callback) {
    $.ajax({
        url: url,
        data: mydata,
        type: 'post',
        cache: false,
        dataType: 'json',
        success: function (data) {
            if (data.success == true) {
                alert("操作成功！");
                if (callback != null) {
                    callback();
                }
            }
            else {
                alert(data.errorMsg);
            }
        },
        error: function (e) {
            // view("异常！");
            alert("操作错误！请重试");
        }
    });
}

function AjaxGetData(url, mydata, callback) {
    $.ajax({
        url: url,
        data: mydata,
        type: 'GET',
        cache: false,
        async: false,
        dataType: 'json',
        success: function (data) {
            if (callback != null) {
                callback(data);
            }
        },
        error: function (e) {
            // view("异常！");
            alert("操作错误！请重试");
        }
    });
}

function OpenFileUploadDialog(dialogName, url, caption,fileid, loadCallBack) {
    var mydialog = $("#" + dialogName);
    mydialog.load(url,
    function () {
        var form = mydialog.find("form");
        mydialog.dialog('option', 'buttons', { "确定": function () { FileUpload(url, form, fileid, loadCallBack); }, "关闭": function () { $(this).dialog("close"); } });
        //mydialog.dialog('option', 'buttons', { "关闭": function () { $(this).dialog("close"); } });
        mydialog.dialog({ title: caption });
        mydialog.dialog("open");
       
    });
}

function FileUpload(url, form, fileid, loadCallBack) {
    form.validate({
        //debug: true,//只验证不提交表单
        submitHandler: function () {
            var data = form.serializeObject();
            $.ajaxFileUpload
            ({
                url: url,// 跳转到 action
                secureuri: false,
                data: data,
                fileElementId: fileid,
                type: 'post',
                cache: false,
                dataType: 'json',
                success: function (mydata) {
                    if (loadCallBack != null) {
                        loadCallBack(mydata);
                    }
                },
                error: function (data, status, e) {
                    // view("异常！");
                    console.log(data);
                    console.log(status);
                    console.log(e);
                    alert("操作错误！请重试" + e);
                }
            });
        }
    });

    //if (form.length > 0) {
    //    form.validate({
    //        debug: true
    //    });
       form.submit();

    //    return;
    //}
   
}

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};