﻿using System.Net;

using Domain.Services.Tasks;
using Common.Engine;
using Common;
using System.Web;

namespace Domain.Services.Common
{
    /// <summary>
    /// Represents a task for keeping the site alive
    /// </summary>
    public partial class KeepAliveTask : ITask
    {
       
        public KeepAliveTask()
        {
        }

        /// <summary>
        /// Executes a task
        /// </summary>
        public void Execute()
        {
            string url = EngineContext.Current.SiteUrl;// HttpContext.Current.Request.Url.ToString();
            //string url = nopConfig.SiteUrl + "keepalive/index";
            using (var wc = new WebClient())
            {
                wc.DownloadString(url);
            }
        }
    }
}
