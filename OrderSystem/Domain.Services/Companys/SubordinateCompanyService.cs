﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Model.Comapnys;
using Domain.Model.Users;
using Domain.Repositories;
using Common;
using Domain.Model.Common;

namespace Domain.Services.Companys
{
    public class SubordinateCompanyService : ISubordinateCompanyService
    {
        IRepository<SubordinateCompany> _repostiory;
        public SubordinateCompanyService(IRepository<SubordinateCompany> repostiory)
        {
            _repostiory = repostiory;
        }

        public SubordinateCompany GetSubCompanyById(int id)
        {
            return _repostiory.Table.Where(p => p.Id == id).FirstOrDefault();
        }

        public IList<SubordinateCompany> GetSubCompanyByType(UserAccount currentUser, SubordinateComapnyType comapnyType)
        {
            if(comapnyType > 0)
                return _repostiory.Table.Where(p=>p.CompanyType==(int)comapnyType).ToList();
            else
                return _repostiory.Table.OrderByDescending(p => p.CreateTime).ToList();
        }

        //分页
        public IPagedList<SubordinateCompany> GetSubordinateCompanyList(SubordinateCompany subordinateCompany, int pageIndex, int pageSize)
        {
            var query = _repostiory.Table.OrderByDescending(p => p.CreateTime);
            if (!string.IsNullOrEmpty(subordinateCompany.Name))
                query = (IOrderedQueryable<SubordinateCompany>)query.Where(p => p.Name.Contains(subordinateCompany.Name));
            if (subordinateCompany.Status > 0)
                query = (IOrderedQueryable<SubordinateCompany>)query.Where(p => p.Status == subordinateCompany.Status);
            if (subordinateCompany.CompanyType > 0)
                query = (IOrderedQueryable<SubordinateCompany>)query.Where(p => p.CompanyType == subordinateCompany.CompanyType);

            return new PagedList<SubordinateCompany>(query, pageIndex, pageSize);
        }

        public void AddSubordinateCompany(SubordinateCompany user)
        {
            user.CreateTime = DateTime.Now;
            _repostiory.Insert(user);
        }

        public void DelSubordinateCompany(int id)
        {
            var user = GetSubCompanyById(id);
            _repostiory.Delete(user);

        }

        public void UpdSubordinateCompany(SubordinateCompany user)
        {
            var model = GetSubCompanyById(user.Id);

            if (!string.IsNullOrEmpty(user.Name))
                model.Name = user.Name;
            if (!string.IsNullOrEmpty(user.Num))
                model.Num = user.Num;
            if (!string.IsNullOrEmpty(user.Contacts))
                model.Contacts = user.Contacts;
            if (!string.IsNullOrEmpty(user.ContactTel1))
                model.ContactTel1 = user.ContactTel1;
            if (!string.IsNullOrEmpty(user.ContactTel2))
                model.ContactTel2 = user.ContactTel2;
            if (!string.IsNullOrEmpty(user.Province))
                model.Province = user.Province;
            if (!string.IsNullOrEmpty(user.City))
                model.City = user.City;
            if (!string.IsNullOrEmpty(user.County))
                model.County = user.County;
            if(!string.IsNullOrEmpty(user.Address))
                model.Address = user.Address; 
                if (!string.IsNullOrEmpty(user.Remarks))
                model.Remarks = user.Remarks;
            if (user.Status > 0)
                model.Status = user.Status;
            if (user.ParentId > 0)
                model.ParentId = user.ParentId;
            if (user.CompanyType > 0)
                model.CompanyType = user.CompanyType;
            if (user.CompanyId > 0)
                model.CompanyId = user.CompanyId;

            _repostiory.Update(model);
        }

        public void StopSubordinateCompany(int id)
        {
            var user = GetSubCompanyById(id);
            if (user.Status == (int)Status.Pending)
                user.Status = (int)Status.Submit;
            //else
            //    user.UserState = (int)UserStatus.Pending;
            _repostiory.Update(user);
        }

    }
}
