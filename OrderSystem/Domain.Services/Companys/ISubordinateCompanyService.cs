﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Comapnys;
using Domain.Model.Users;
using Common;

namespace Domain.Services.Companys
{
    public interface ISubordinateCompanyService
    {
        IList<SubordinateCompany> GetSubCompanyByType(UserAccount currentUser, SubordinateComapnyType fWS);

        IPagedList<SubordinateCompany> GetSubordinateCompanyList(SubordinateCompany subordinateCompany, int pageIndex, int pageSize);
        SubordinateCompany GetSubCompanyById(int id);
        void AddSubordinateCompany(SubordinateCompany user);
        void DelSubordinateCompany(int id);
        void UpdSubordinateCompany(SubordinateCompany user);
        void StopSubordinateCompany(int id);
    }
}
