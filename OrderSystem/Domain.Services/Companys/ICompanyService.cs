﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Comapnys;
using Domain.Model.Users;
using Common;

namespace Domain.Services.Comapnys
{
    public interface ICompanyService
    {
        IPagedList<Comapny> GetComapnyList(Comapny Comapny, int pageIndex, int pageSize);
        Comapny GetById(int id);
        //void AddComapny(Comapny user);
        //void DelComapny(int id);
        //void UpdComapny(Comapny user);
        //void StopComapny(int id);
    }
}
