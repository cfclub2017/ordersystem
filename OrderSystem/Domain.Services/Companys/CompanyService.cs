﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Model.Comapnys;
using Domain.Model.Users;
using Domain.Repositories;
using Common;
using Domain.Model.Common;

namespace Domain.Services.Comapnys
{
    public class CompanyService : ICompanyService
    {
        IRepository<Comapny> _repostiory;
        public CompanyService(IRepository<Comapny> repostiory)
        {
            _repostiory = repostiory;
        }

        public Comapny GetById(int id)
        {
            return _repostiory.Table.Where(p => p.Id == id).FirstOrDefault();
        }

        //分页
        public IPagedList<Comapny> GetComapnyList(Comapny Comapny, int pageIndex, int pageSize)
        {
            var query = _repostiory.Table.OrderByDescending(p => p.CreateTime);
            //if (!string.IsNullOrEmpty(Comapny.Name))
            //    query = (IOrderedQueryable<Comapny>)query.Where(p => p.Name.Contains(Comapny.Name));
            //if (Comapny.Status > 0)
            //    query = (IOrderedQueryable<Comapny>)query.Where(p => p.Status == Comapny.Status);
            //if (Comapny.ComapnyType > 0)
            //    query = (IOrderedQueryable<Comapny>)query.Where(p => p.ComapnyType == Comapny.ComapnyType);
            //var query = _repostiory.Table;
            return new PagedList<Comapny>(query, pageIndex, pageSize);
        }

        //public void AddComapny(Comapny user)
        //{
        //    user.CreateTime = DateTime.Now;
        //    _repostiory.Insert(user);
        //}

        //public void DelComapny(int id)
        //{
        //    var user = GetSubComapnyById(id);
        //    _repostiory.Delete(user);

        //}

        //public void UpdComapny(Comapny user)
        //{
        //    var model = GetSubComapnyById(user.Id);

        //    if (!string.IsNullOrEmpty(user.Name))
        //        model.Name = user.Name;
        //    if (!string.IsNullOrEmpty(user.Num))
        //        model.Num = user.Num;
        //    if (!string.IsNullOrEmpty(user.Contacts))
        //        model.Contacts = user.Contacts;
        //    if (!string.IsNullOrEmpty(user.ContactTel1))
        //        model.ContactTel1 = user.ContactTel1;
        //    if (!string.IsNullOrEmpty(user.ContactTel2))
        //        model.ContactTel2 = user.ContactTel2;
        //    if (!string.IsNullOrEmpty(user.Province))
        //        model.Province = user.Province;
        //    if (!string.IsNullOrEmpty(user.City))
        //        model.City = user.City;
        //    if (!string.IsNullOrEmpty(user.County))
        //        model.County = user.County;
        //    if(!string.IsNullOrEmpty(user.Address))
        //        model.Address = user.Address; 
        //        if (!string.IsNullOrEmpty(user.Remarks))
        //        model.Remarks = user.Remarks;
        //    if (user.Status > 0)
        //        model.Status = user.Status;
        //    if (user.ParentId > 0)
        //        model.ParentId = user.ParentId;
        //    if (user.ComapnyType > 0)
        //        model.ComapnyType = user.ComapnyType;
        //    if (user.ComapnyId > 0)
        //        model.ComapnyId = user.ComapnyId;

        //    _repostiory.Update(model);
        //}

        //public void StopComapny(int id)
        //{
        //    var user = GetSubComapnyById(id);
        //    if (user.Status == (int)Status.Pending)
        //        user.Status = (int)Status.Submit;
        //    //else
        //    //    user.UserState = (int)UserStatus.Pending;
        //    _repostiory.Update(user);
        //}

    }
}
