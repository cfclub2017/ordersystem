﻿
//using Domain.Model.Directory;


using Domain.Model.Users;

namespace Domain.Services
{
    /// <summary>
    /// Work context
    /// </summary>
    public interface IWorkContext
    {

        /// <summary>
        /// Gets or sets the current customer
        /// </summary>
        UserAccount CurrentUser { get; set; }
    }
}
