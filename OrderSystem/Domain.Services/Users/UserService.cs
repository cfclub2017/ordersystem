﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Users;

using Domain.Repositories;
using Common;

namespace Domain.Services.Users
{
    public class UserService:IUserService
    {
        IRepository<UserAccount> _userRepository;
        public UserService(IRepository<UserAccount> userRepository)
        {
            _userRepository = userRepository;
        }

        public UserAccount GetUserById(int userId)
        {
            return _userRepository.Table.Where(p => p.Id == userId).FirstOrDefault();
        }

        public IList<UserAccount> GetUserByType(UserAccount currentUser, UserType dG)
        {
            return _userRepository.Table.Where(p =>  p.UserType == (int)dG).ToList();
        }

        public UserAccount GetUserByUsername(string userName)
        {
           return _userRepository.Table.Where(p => p.UserName == userName).FirstOrDefault();
        }

        //分页
        public IPagedList<UserAccount> GetUserAccountList(UserAccount currentUser, int pageIndex, int pageSize)
        {
            //var query = _userRepository.Table.OrderByDescending(p => p.CreateTime);
            var query = _userRepository.Table.OrderByDescending(p => p.CreateTime); ;
            if (!string.IsNullOrEmpty(currentUser.UserName))
                query = (IOrderedQueryable<UserAccount>)query.Where(p => p.UserName.Contains(currentUser.UserName));
            if (currentUser.UserState > 0)
                query = (IOrderedQueryable<UserAccount>)query.Where(p => p.UserState == currentUser.UserState);
            if (currentUser.RoleID > 0)
                query = (IOrderedQueryable<UserAccount>)query.Where(p => p.RoleID == currentUser.RoleID);
            //query.OrderByDescending(p => p.CreateTime);
            return new PagedList<UserAccount>(query, pageIndex, pageSize);
        }

        public void AddUserAccount(UserAccount user)
        {
            user.CreateTime = DateTime.Now;
            _userRepository.Insert(user);
        }

        public void DelUserAccount(int id)
        {
            var user = GetUserById(id);
            _userRepository.Delete(user);
                
        }

        public void UpdUserAccount(UserAccount user)
        {
            var model = GetUserById(user.Id);
            
            if (!string.IsNullOrEmpty(user.Name))
                model.Name = user.Name;
            if (!string.IsNullOrEmpty(user.UserName))
                model.UserName = user.UserName;
            if (user.UserState>0)
                model.UserState = user.UserState;
            if (user.UserType > 0)
                model.UserType = user.UserType;
            if (!string.IsNullOrEmpty(user.Tel))
                model.Tel = user.Tel;
            if (user.CompanyID>0)
                model.CompanyID = user.CompanyID;
            model.CompanyID = user.CompanyID;
            if (!string.IsNullOrEmpty(user.UserPwd))
                model.UserPwd = user.UserPwd;
            _userRepository.Update(model);
        }

        public void StopUserAccount(int id)
        {
            var user = GetUserById(id);
            if (user.UserState == (int)UserStatus.Pending)
                user.UserState = (int)UserStatus.Submit;
            //else
            //    user.UserState = (int)UserStatus.Pending;
            _userRepository.Update(user);
        }

    }
}
