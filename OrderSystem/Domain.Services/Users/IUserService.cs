﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Domain.Model.Users;
using Common;

namespace Domain.Services.Users
{
    public interface IUserService
    {
        UserAccount GetUserByUsername(string userName);
        UserAccount GetUserById(int userId);
        IList<UserAccount> GetUserByType(UserAccount currentUser, UserType dG);
        IPagedList<UserAccount> GetUserAccountList(UserAccount currentUser, int pageIndex, int pageSize);
        void AddUserAccount(UserAccount user);
        void DelUserAccount(int id);
        void UpdUserAccount(UserAccount user);
        void StopUserAccount(int id);
    }
}
