﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Domain.Model.Users;
using Domain.Model.Products;
using Common;

namespace Domain.Services.Users
{
    public interface IMenuService
    {
        Menu GetById(int Id);
        IPagedList<Menu> GetMenuList(Menu Menu, int pageIndex, int pageSize);
        List<Menu> GetAllMenu();
        void Add(Menu model);
        void Del(int id);
        void Upd(Menu model);
    }
}
