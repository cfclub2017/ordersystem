﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Domain.Model.Users;
using Domain.Model.Products;
using Common;

namespace Domain.Services.Users
{
    public interface IRoleService
    {
        Role GetRoleByUser(UserAccount user);
        Role GetById(int Id);
        IPagedList<Role> GetRoleList(Role Role, int pageIndex, int pageSize);
        void Add(Role model);
        void Del(int id);
        void Upd(Role model);
        IList<Role> GetAll();
    }
}
