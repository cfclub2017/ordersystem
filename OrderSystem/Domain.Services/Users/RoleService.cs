﻿using System;
using System.Linq;
using Common;
using Domain.Repositories;
using Domain.Model.Users;
using System.Collections.Generic;

namespace Domain.Services.Users
{
    public class RoleService : IRoleService
    {
        IRepository<Role> _repostiory;
        public RoleService(IRepository<Role> repostiory)
        {
            _repostiory = repostiory;
        }

        public Role GetRoleByUser(UserAccount user)
        {
            return _repostiory.Table.Where(p => p.Id == user.RoleID).FirstOrDefault();
        }

        public Role GetById(int Id)
        {
            return _repostiory.Table.Where(p => p.Id == Id).FirstOrDefault();
        }

        public IList<Role> GetAll()
        {
            return _repostiory.Table.ToList();
        }

        public IPagedList<Role> GetRoleList(Role Role, int pageIndex, int pageSize)
        {
            var query = _repostiory.Table.OrderByDescending(p => p.CreateTime); ;
            if (!string.IsNullOrEmpty(Role.Name))
                query = (IOrderedQueryable<Role>)query.Where(p => p.Name.Contains(Role.Name));
            return new PagedList<Role>(query, pageIndex, pageSize);
        }

        public void Add(Role model)
        {
            model.CreateTime = DateTime.Now;
            _repostiory.Insert(model);
        }

        public void Del(int id)
        {
            var model = GetById(id);
            _repostiory.Delete(model);
        }

        public void Upd(Role Role)
        {
            var model = GetById(Role.Id);

            if (!string.IsNullOrEmpty(Role.Name))
                model.Name = Role.Name;
            if (!string.IsNullOrEmpty(Role.Remarks))
                model.Remarks = Role.Remarks;
            if (!string.IsNullOrEmpty(Role.Code))
                model.Code = Role.Code;
            _repostiory.Update(model);
        }
    }
}
