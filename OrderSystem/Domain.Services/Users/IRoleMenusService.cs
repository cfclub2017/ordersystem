﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Domain.Model.Users;
using Domain.Model.Products;
using Common;

namespace Domain.Services.Users
{
    public interface IRoleMenusService
    {
        //全量更新，先删后加
        void Upd(List<RoleMenus> r,int RoleID);
        List<RoleMenus> GetByID(int RoleID);
        void UpdOne(RoleMenus r, int RoleID);
    }
}
