﻿using System;
using System.Linq;
using Common;
using Domain.Repositories;
using Domain.Model.Users;
using System.Collections.Generic;

namespace Domain.Services.Users
{
    public class MenuService : IMenuService
    {
        IRepository<Menu> _repostiory;
        public MenuService(IRepository<Menu> repostiory)
        {
            _repostiory = repostiory;
        }

        public Menu GetById(int Id)
        {
            return _repostiory.Table.Where(p => p.Id == Id).FirstOrDefault();
        }

        public List<Menu> GetAllMenu() {
            return _repostiory.Table.ToList();
        }

        public IPagedList<Menu> GetMenuList(Menu Menu, int pageIndex, int pageSize)
        {
            var query = _repostiory.Table.OrderByDescending(p => p.CreateTime); ;
            if (!string.IsNullOrEmpty(Menu.Name))
                query = (IOrderedQueryable<Menu>)query.Where(p => p.Name.Contains(Menu.Name));
            if (Menu.Status>0)
                query = (IOrderedQueryable<Menu>)query.Where(p => p.Status == Menu.Status);
            return new PagedList<Menu>(query, pageIndex, pageSize);
        }

        public void Add(Menu model)
        {
            model.CreateTime = DateTime.Now;
            _repostiory.Insert(model);
        }

        public void Del(int id)
        {
            var model = GetById(id);
            _repostiory.Delete(model);
        }

        public void Upd(Menu Menu)
        {
        var model = GetById(Menu.Id);
            if (!string.IsNullOrEmpty(Menu.Name))
                model.Name = Menu.Name;
            if (!string.IsNullOrEmpty(Menu.Remarks))
                model.Remarks = Menu.Remarks;
            if (!string.IsNullOrEmpty(Menu.ActionName))
                model.ActionName = Menu.ActionName;
            if (!string.IsNullOrEmpty(Menu.ControllerName))
                model.ControllerName = Menu.ControllerName;
            if (!string.IsNullOrEmpty(Menu.IconClass))
                model.IconClass = Menu.IconClass;
            if (Menu.OrderNo > 0)
                model.OrderNo = Menu.OrderNo;
            if (Menu.ParentId > 0)
                model.ParentId = Menu.ParentId;
            if (Menu.Status > 0)
                model.Status = Menu.Status;
            if (Menu.IfShow > 0)
                model.IfShow = Menu.IfShow;
            _repostiory.Update(model);
        }
    }
}
