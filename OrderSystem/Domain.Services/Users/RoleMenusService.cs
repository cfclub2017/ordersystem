﻿using System;
using System.Linq;
using Common;
using Domain.Repositories;
using Domain.Model.Users;
using System.Collections.Generic;

namespace Domain.Services.Users
{
    public class RoleMenusService : IRoleMenusService
    {
        IRepository<RoleMenus> _repostiory;
        public RoleMenusService(IRepository<RoleMenus> repostiory)
        {
            _repostiory = repostiory;
        }

        List<RoleMenus> IRoleMenusService.GetByID(int RoleID)
        {
          return _repostiory.Table.Where(p => p.RoleID == RoleID).ToList();
        }

        void IRoleMenusService.Upd(List<RoleMenus> r, int RoleID)
        {
            List<RoleMenus> extList = _repostiory.Table.Where(p => p.RoleID == RoleID).ToList();
            if(extList.Count > 0)
                _repostiory.Delete(extList);
            _repostiory.Insert(r);
        }

        void IRoleMenusService.UpdOne(RoleMenus r, int RoleID)
        {
            _repostiory.Insert(r);
        }
    } }
