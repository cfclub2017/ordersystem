﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Users;
using Domain.Model.Stores;
using Common;

namespace Domain.Services.Stores
{
    public interface IInStoreService
    {
        IPagedList<InStore> GetInStoreOrderList(UserAccount currentUser, int pageIndex, int pageSize);
    }
}
