﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Users;
using Domain.Model.Stores;

namespace Domain.Services.Stores
{
    public interface IStoreService
    {
        IList<Store> GetStore(UserAccount currentUser);
    }
}
