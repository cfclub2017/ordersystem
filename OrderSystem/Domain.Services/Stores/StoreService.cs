﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Stores;
using Domain.Model.Users;
using Domain.Repositories;

namespace Domain.Services.Stores
{
    public class StoreService : IStoreService
    {
        IRepository<Store> _repostiory;
        public StoreService(IRepository<Store> repostiory)
        {
            _repostiory = repostiory;
        }
      
        public IList<Store> GetStore(UserAccount currentUser)
        {
           return _repostiory.Table.ToList();
        }
    }
}
