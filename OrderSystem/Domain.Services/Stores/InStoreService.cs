﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Stores;
using Domain.Model.Users;
using Domain.Repositories;
using Common;

namespace Domain.Services.Stores
{
    public class InStoreService : IInStoreService
    {
        IRepository<InStore> _repostiory;
        public InStoreService(IRepository<InStore> repostiory)
        {
            _repostiory = repostiory;
        }

        public IPagedList<InStore> GetInStoreOrderList(UserAccount currentUser, int pageIndex, int pageSize)
        {
            var query = _repostiory.Table.OrderByDescending(p => p.CreateTime);
            return new PagedList<InStore>(query, pageIndex, pageSize);
        }
    }
}
