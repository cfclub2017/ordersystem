﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Domain.Model.Products;
using Domain.Model.Stores;
using Domain.Model.Users;
using Domain.Repositories;

namespace Domain.Services.Products
{
    public class ProductService : IProductService
    {
        IRepository<Product> _repostiory;
        IRepository<ProductStore> _productStoreRepostiory;
        public ProductService(IRepository<Product> repostiory, IRepository<ProductStore> productStoreRepostiory)
        {
            _repostiory = repostiory;
            _productStoreRepostiory = productStoreRepostiory;
        }
        public Product GetById(int userId)
        {
            return _repostiory.Table.Where(p => p.Id == userId).FirstOrDefault();
        }
        public IList<ProductStore> getAllProductStore(UserAccount currentUser, int pageIndex, int pageSize)
        {
            var query = _productStoreRepostiory.Table.OrderByDescending(p => p.Id);
            return new PagedList<ProductStore>(query, pageIndex, pageSize);
        }

        public IList<Product> getProductByCatalog(int id)
        {
            return _repostiory.Table.Where(p => p.CatalogId == id).ToList();
        }

        public Product getProductById(int id)
        {
            return _repostiory.Table.Where(p => p.Id == id).FirstOrDefault();
        }

        public void Add(Product model)
        {
            model.CreateTime = DateTime.Now;
            _repostiory.Insert(model);
        }

        public void Del(int id)
        {
            var model = GetById(id);
            _repostiory.Delete(model);
        }

        public void Upd(Product product)
        {
            var model = GetById(product.Id);

            if (!string.IsNullOrEmpty(product.Name))
                model.Name = product.Name;
            if (!string.IsNullOrEmpty(product.Remarks))
                model.Remarks = product.Remarks;
            if (!string.IsNullOrEmpty(product.Num))
                model.Num = product.Num;
            if (product.Img != null)
                model.Img = product.Img;
            if (product.CostPrice >= 0)
                model.CostPrice = product.CostPrice;
            if (product.CatalogId > 0)
                model.CatalogId = product.CatalogId;
            if (product.ParentCataloglogId > 0)
                model.ParentCataloglogId = product.ParentCataloglogId;
            model.Catalog = product.Catalog;
            if (!string.IsNullOrEmpty(product.Specification))
                model.Specification = product.Specification;
            if (product.SuggestPrice >= 0)
                model.SuggestPrice = product.SuggestPrice;
            if (product.MaxPrice >= 0)
                model.MaxPrice = product.MaxPrice;
            if (product.MinPrice >= 0)
                model.MinPrice = product.MinPrice;
            if (product.CRMNum >=0 )
                model.CRMNum = product.CRMNum;
                //model.UserId = product.UserId;
            if (!string.IsNullOrEmpty(product.Cover))
                model.Cover = product.Cover;
            if (product.Status >= 0)
                model.Status = product.Status;
            model.Id = product.Id;
            _repostiory.Update(model);
        }

        public IPagedList<Product> GetProductList(Product product, int pageIndex, int pageSize)
        {
            var query = _repostiory.Table.OrderByDescending(p => p.CreateTime);
            if (!string.IsNullOrEmpty(product.Num))
                query = (IOrderedQueryable<Product>)query.Where(p => p.Num.Contains(product.Num));
            if (!string.IsNullOrEmpty(product.Name))
                query = (IOrderedQueryable<Product>)query.Where(p => p.Name.Contains(product.Name));
            if (!string.IsNullOrEmpty(product.Specification))
                query = (IOrderedQueryable<Product>)query.Where(p => p.Specification.Contains(product.Specification));
            if (product.Status > 0)
                query = (IOrderedQueryable<Product>)query.Where(p => p.Status == product.Status);
            if(product.CatalogId > 0)
                query = (IOrderedQueryable<Product>)query.Where(p => p.CatalogId == product.CatalogId);
            if (product.ParentCataloglogId > 0)
                query = (IOrderedQueryable<Product>)query.Where(p => p.Catalog.ParentId == product.ParentCataloglogId);
            return new PagedList<Product>(query, pageIndex, pageSize);
        }

        public IPagedList<ProductStore> getAllProductStore(UserAccount currentUser, string productName, int storeId, int catalogId, int pageIndex, int pageSize)
        {
            var query = _productStoreRepostiory.Table;
            if (!string.IsNullOrEmpty(productName))
            {
                query = query.Where(p => p.Product.Name.Contains(productName));
            }
            if (storeId != -1)
            {
                query = query.Where(p => p.StoreId == storeId);
            }
            if (catalogId != -1)
            {
                query = query.Where(p => p.Product.CatalogId == catalogId);
            }
            query = query.OrderByDescending(p => p.Id);
            return new PagedList<ProductStore>(query, pageIndex, pageSize);
        }
    }
}
