﻿using Common;
using Domain.Model.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Users;
using Domain.Model.Stores;

namespace Domain.Services.Products
{
    public interface IProductService
    {
        IList<Product> getProductByCatalog(int id);
        Product GetById(int userId);
        Product getProductById(int id);
        IList<ProductStore> getAllProductStore(UserAccount currentUser, int pageIndex, int pageSize);
        IPagedList<ProductStore> getAllProductStore(UserAccount currentUser, string productName, int storeId, int catalogId, int pageIndex, int pageSize);

        IPagedList<Product> GetProductList(Product product, int pageIndex, int pageSize);
        void Add(Product model);
        void Del(int id);
        void Upd(Product model);
    }
}
