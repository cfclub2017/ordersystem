﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Domain.Model.Users;
using Domain.Model.Products;
using Common;

namespace Domain.Services.Products
{
    public interface ICatalogService
    {
        List<Catalog> GetAllCataLog(UserAccount currentUser);
        Catalog GetById(int userId);
        IPagedList<Catalog> GetCataLogList(Catalog catalog, int pageIndex, int pageSize);
        void Add(Catalog model);
        void Del(int id);
        void Upd(Catalog model);
        List<Catalog> GetAllCataLogByParm(Catalog catalog);
    }
}
