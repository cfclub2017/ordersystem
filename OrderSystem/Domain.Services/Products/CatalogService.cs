﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Domain.Model.Products;
using Domain.Model.Users;
using Domain.Repositories;

namespace Domain.Services.Products
{
    public class CatalogService : ICatalogService
    {
        IRepository<Catalog> _repostiory;
        public CatalogService(IRepository<Catalog> repostiory)
        {
            _repostiory = repostiory;
        }

        public List<Catalog> GetAllCataLog(UserAccount currentUser)
        {
            return _repostiory.Table.Where(p => p.CompanyId == currentUser.CompanyID).ToList();
        }

        public List<Catalog> GetAllCataLogByParm(Catalog catalog)
        {
            var query = _repostiory.Table;
            if (!string.IsNullOrEmpty(catalog.Name))
                query = (IOrderedQueryable<Catalog>)query.Where(p => p.Name.Contains(catalog.Name));
            if (catalog.Level > 0)
                query = (IOrderedQueryable<Catalog>)query.Where(p => p.Level == catalog.Level);
            return query.ToList() ;
        }

        public Catalog GetById(int userId)
        {
            return _repostiory.Table.Where(p => p.Id == userId).FirstOrDefault();
        }

        public IPagedList<Catalog> GetCataLogList(Catalog catalog, int pageIndex, int pageSize)
        {
            var query = _repostiory.Table.OrderByDescending(p => p.CreateTime); ;
            if (!string.IsNullOrEmpty(catalog.Name))
                query = (IOrderedQueryable<Catalog>)query.Where(p => p.Name.Contains(catalog.Name));
            if (catalog.Level>0)
                query = (IOrderedQueryable<Catalog>)query.Where(p => p.Level == catalog.Level);
            if(catalog.ParentId > 0)
                query = (IOrderedQueryable<Catalog>)query.Where(p => p.ParentId == catalog.ParentId);
            return new PagedList<Catalog>(query, pageIndex, pageSize);
        }

        public List<Catalog> GetCataLogListByLevel(Catalog catalog)
        {
            if (catalog.Level > 0)
                return _repostiory.Table.Where(p => p.Level == catalog.Level).ToList();
            return null;
        }

        public void Add(Catalog model)
        {
            model.CreateTime = DateTime.Now;
            _repostiory.Insert(model);
        }

        public void Del(int id)
        {
            var model = GetById(id);
            _repostiory.Delete(model);
        }

        public void Upd(Catalog catalog)
        {
            var model = GetById(catalog.Id);

            if (!string.IsNullOrEmpty(catalog.Name))
                model.Name = catalog.Name;
            if (!string.IsNullOrEmpty(catalog.Remarks))
                model.Remarks = catalog.Remarks;
            if (catalog.ParentId > 0)
                model.ParentId = catalog.ParentId;
            if (catalog.CompanyId > 0)
                model.CompanyId = catalog.CompanyId;
            if (catalog.Level > 0)
                model.Level = catalog.Level;
            if (catalog.Icon != null)
                model.Icon = catalog.Icon;
            if (!string.IsNullOrEmpty(catalog.ImageMimeType))
                model.ImageMimeType = catalog.ImageMimeType;
            if (!string.IsNullOrEmpty(catalog.CompanyName))
                model.CompanyName = catalog.CompanyName;
            if (!string.IsNullOrEmpty(catalog.ParentName))
                model.ParentName = catalog.ParentName;
            if (!string.IsNullOrEmpty(catalog.Num))
                model.Num = catalog.Num;
            _repostiory.Update(model);
        }
    }
}
