﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Users;
using Common;
using Domain.Model.Orders;

namespace Domain.Services.Orders
{
    public interface IOrderService
    {
        IPagedList<Order> GetOrderList(UserAccount currentUser, int pageIndex, int pageSize);
        void AddOrder(Order order);
        Order GetOrderById(int id);
        IPagedList<OrderItem> GetOrderProducts(int id, int pageIndex, int pageSize);
        void AddOrderProduct(OrderItem data);
        void DelOrderProduct(int id);
        OrderItem GetOrderProductById(int id);
        void UpdOrderProduct(int id, int productId, int quantity, string remarks);
        void UpdOrderStatus(int orderId,int status);
        void UpdFWS(int orderId, int fWSID);
        void ConfirmInvoice(int orderId,int ctype);
        IPagedList<Order> GetOrdersByStatus(UserAccount currentUser, OrderStatus delivery, int pageIndex, int pageSize);

        IPagedList<OrderItem> GetOrderItems(UserAccount currentUser, int pageIndex, int pageSize);
        void AddOrderImg(int id, string imgPath);
        IPagedList<object> GetDayOrders(UserAccount currentUser, DateTime? statrTime, DateTime? endTime,string status, int pageIndex, int pageSize);
        IPagedList<object> GetMonthOrders(UserAccount currentUser, DateTime? startTime, DateTime? endTime, string status, int pageIndex, int pageSize);
        IPagedList<Order> GetAllOrders(UserAccount currentUser, string orderNum, string status, int pageIndex, int pageSize);
    }
}
