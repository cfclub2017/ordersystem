﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Model.Users;
using Domain.Repositories;
using Domain.Model.Orders;
using Common;
using Common.Engine;
using Domain.Services.Products;

namespace Domain.Services.Orders
{
    public class OrderService : IOrderService
    {
        IRepository<Order> _order;
        IRepository<OrderItem> _orderItemRepository;
        public OrderService(IRepository<Order> order, IRepository<OrderItem> orderItemRepository)
        {
            _order = order;
            _orderItemRepository = orderItemRepository;
        }

        public void AddOrder(Order order)
        {
            _order.Insert(order);
        }

        public void AddOrderImg(int id, string imgPath)
        {
            throw new NotImplementedException();
        }

        public void AddOrderProduct(OrderItem data)
        {
            _orderItemRepository.Insert(data);
        }

        public void ConfirmInvoice(int orderId,int ctype)
        {
            var order=_order.GetById(orderId);
            if (ctype == 0)
            {
                order.FWSUserSubmitStatus = 1;
                order.FWSUserSubmitTime = DateTime.Now;
            }
            if(ctype==1)
            {
                order.FWSSubmitStatus = 1;
                order.FWSSubmitTime = DateTime.Now;
            }
            _order.Update(order);
        }


        public void DelOrderProduct(int id)
        {
            var orderItem = GetOrderProductById(id);
            _orderItemRepository.Delete(orderItem);
        }

        public IPagedList<Order> GetAllOrders(UserAccount currentUser, string orderNum, string status, int pageIndex, int pageSize)
        {
            var query = _order.Table;
            if (!string.IsNullOrEmpty(orderNum))
            {
                query = query.Where(p => p.OrderNum.Contains(orderNum) || p.CustomerTel1 == orderNum);
            }
            if (!string.IsNullOrEmpty(status) && status != "-1")
            {
                int statusId = Convert.ToInt32(status);
                query = query.Where(p => p.Status == statusId);
            }
            query = query.OrderByDescending(p => p.CreateTime);
            return new PagedList<Order>(query, pageIndex, pageSize);
        }

        public IPagedList<object> GetDayOrders(UserAccount currentUser, DateTime? statrTime, DateTime? endTime, string status, int pageIndex, int pageSize)
        {
            var query = _order.Table;
            if (statrTime != null && endTime != null)
            {
                query = query.Where(p => p.CreateTime >= statrTime && p.CreateTime < endTime);
            }
            if (!string.IsNullOrEmpty(status)&&status!="-1")
            {
                int statusId = Convert.ToInt32(status);
                query = query.Where(p => p.Status == statusId);
            }
            var querynew = query.GroupBy(p => new { p.CreateTime.Year, p.CreateTime.Month, p.CreateTime.Day })
                 .Select(p => new
                 {
                     Day = p.Key.Year + "-" + p.Key.Month + "-" + p.Key.Day,
                     Count = p.Count(),
                     OrderItemsCount = (int?)p.Sum(a => a.OrderItems.Sum(c => c.Quantity)),
                     Totals = (int?)p.Sum(a => a.OrderItems.Sum(c => c.Price)),
                 }).OrderByDescending(p => p.Day);
            return new PagedList<object>(querynew, pageIndex, pageSize);
        }

        public IPagedList<object> GetMonthOrders(UserAccount currentUser, DateTime? startTime, DateTime? endTime, string status, int pageIndex, int pageSize)
        {
            var query = _order.Table;
            if (startTime != null && endTime != null)
            {
                query = query.Where(p => p.CreateTime >= startTime && p.CreateTime < endTime);
            }
            if (!string.IsNullOrEmpty(status))
            {
                int statusId = Convert.ToInt32(status);
                query = query.Where(p => p.Status == statusId);
            }
            var querynew = query.GroupBy(p => new { p.CreateTime.Year, p.CreateTime.Month })
                 .Select(p => new
                 {
                     Day = p.Key.Year + "-" + p.Key.Month,
                     Count = p.Count(),
                     OrderItemsCount = (int?)p.Sum(a => a.OrderItems.Sum(c => c.Quantity)),
                     Totals = (int?)p.Sum(a => a.OrderItems.Sum(c => c.Price)),
                 }).OrderByDescending(p => p.Day);
            return new PagedList<object>(querynew, pageIndex, pageSize);
        }

        public Order GetOrderById(int id)
        {
            return _order.Table.Where(p=>p.Id==id).FirstOrDefault();
        }

        public IPagedList<OrderItem> GetOrderItems(UserAccount currentUser, int pageIndex, int pageSize)
        {
            var query = _orderItemRepository.Table.OrderByDescending(p => p.Id);
            return new PagedList<OrderItem>(query, pageIndex, pageSize);
        }

        public IPagedList<Order> GetOrderList(UserAccount currentUser,int pageIndex,int pageSize)
        {
            var query = _order.Table.OrderByDescending(p=>p.CreateTime);
            return new PagedList<Order>(query, pageIndex, pageSize);
        }

        public OrderItem GetOrderProductById(int id)
        {
           return _orderItemRepository.Table.Where(p=>p.Id==id).FirstOrDefault();
        }

        public IPagedList<OrderItem> GetOrderProducts(int id, int pageIndex, int pageSize)
        {
            var query = _orderItemRepository.Table.Where(p=>p.OrderId==id).OrderByDescending(p => p.Id);
            return new PagedList<OrderItem>(query, pageIndex, pageSize);
        }

        public IPagedList<Order> GetOrdersByStatus(UserAccount currentUser, OrderStatus delivery, int pageIndex, int pageSize)
        {
            var query = _order.Table.Where(p=>p.Status==(int)delivery).OrderByDescending(p => p.CreateTime);
            return new PagedList<Order>(query, pageIndex, pageSize);
        }

        public void UpdFWS(int orderId, int fWSID)
        {
            var order= _order.GetById(orderId);
            order.FWSID = fWSID;
            _order.Update(order);
        }

        public void UpdOrderProduct(int id, int productId, int quantity, string remarks)
        {
            var productService = EngineContext.Current.Resolve<IProductService>();
            var orderItem = _orderItemRepository.GetById(id);
            var product = productService.getProductById(productId);
            orderItem.ProductId = productId;
            orderItem.Price = product.SuggestPrice;
            orderItem.Quantity = quantity;
            orderItem.Remarks = remarks;
            _orderItemRepository.Update(orderItem);
        }

        public void UpdOrderStatus(int orderId,int status)
        {
           var Order= _order.GetById(orderId);
            Order.Status = status;
            _order.Update(Order);
        }
    }
}
