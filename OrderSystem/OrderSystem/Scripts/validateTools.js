﻿//表单校验相关
var mobile = /^0{0,1}(13[0-9]|15[0-9]|153|156|18[0-9])[0-9]{8}$/;
//alert(mobile.test("18607922180"));
$.validator.addMethod("isMobile", function (value, element) {
    var length = value.length;
    var mobile = /^0{0,1}(13[0-9]|15[0-9]|153|156|18[0-9])[0-9]{8}$/;
    return this.optional(element) || (length == 11 && mobile.test(value));
}, "请正确填写您的手机号码");
// 联系电话(手机/电话皆可)验证

$.validator.addMethod("isPhone", function (value, element) {
    var length = value.length;
    var mobile = /^0{0,1}(13[0-9]|15[0-9]|17[0-9]|153|156|18[0-9])[0-9]{8}$/;
    var tel = /^((0\d{2,3}-\d{7,8})|(1[3584]\d{9}))$/;
    return this.optional(element) || (tel.test(value) || mobile.test(value));
}, "请正确填写您的电话号码");

// 字符验证
jQuery.validator.addMethod("stringCheck", function (value, element) {
    return this.optional(element) || /^[u0391-uFFE5w]+$/.test(value);
}, "只能包括中文字、英文字母、数字和下划线");

// 中文字两个字节
jQuery.validator.addMethod("byteRangeLength", function (value, element, param) {
    var length = value.length;
    for (var i = 0; i < value.length; i++) {
        if (value.charCodeAt(i) > 127) {
            length++;
        }
    }
    return this.optional(element) || (length >= param[0] && length <= param[1]);
}, "请确保输入的值在3 - 15个字节之间(一个中文字算2个字节) ");

// 身份证号码验证
jQuery.validator.addMethod("isIdCardNo", function (value, element) {
    return this.optional(element) || isIdCardNo(value);
}, "请正确输入您的身份证号码");

// 邮政编码验证
jQuery.validator.addMethod("isZipCode", function (value, element) {
    var tel = /^[0-9]{6}$/;
    return this.optional(element) || (tel.test(value));
}, "请正确填写您的邮政编码");