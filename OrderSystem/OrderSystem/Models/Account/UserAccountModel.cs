﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderSystem.Models.Account
{
    public class UserAccountModel : Domain.Model.Users.UserAccount
    {
        public UserAccountModel()
        {
            UserStateList = new List<SelectListItem>();
            UserTypeList = new List<SelectListItem>();
            CompanyList = new List<SelectListItem>();
            UserRoleList = new List<SelectListItem>();
        }

        public IList<SelectListItem> UserStateList { get; set; }
        public IList<SelectListItem> UserTypeList { get; set; }
        public IList<SelectListItem> CompanyList { get; set; }
        public IList<SelectListItem> UserRoleList { get; set; }

        //密码验证
        public string UserCheckPwd { get; set; }
    }
}