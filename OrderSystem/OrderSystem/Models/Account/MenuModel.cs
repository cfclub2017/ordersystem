﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderSystem.Models.Account
{
    public class MenuModel : Domain.Model.Users.Menu
    {
        public MenuModel()
        {
            StatusList = new List<SelectListItem>();
            IfSowList = new List<SelectListItem>();
        }

        public IList<SelectListItem> StatusList { get; set; }
        public IList<SelectListItem> IfSowList { get; set; }

    }
}