﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderSystem.Models.SubordinateCompany
{
    public class SubordinateCompanyModel : Domain.Model.Comapnys.SubordinateCompany
    {
        public SubordinateCompanyModel()
        {
            StatusList = new List<SelectListItem>();
            SubordinateCompanyTypeList = new List<SelectListItem>();
            //总公司
            CompanyList = new List<SelectListItem>();
            //上级公司
            ParCompanyList = new List<SelectListItem>();
        }

        public IList<SelectListItem> StatusList { get; set; }
        public IList<SelectListItem> SubordinateCompanyTypeList { get; set; }
        public IList<SelectListItem> CompanyList { get; set; }
        public IList<SelectListItem> ParCompanyList { get; set; }

    }
}