﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderSystem.Models.Product
{
    public class CatalogModel : Domain.Model.Products.Catalog
    {
        public CatalogModel()
        {
            CompanyList = new List<SelectListItem>();
            ParCatalogList = new List<SelectListItem>();
            LevelList = new List<SelectListItem>();
        }

        public IList<SelectListItem> CompanyList { get; set; }
        public IList<SelectListItem> ParCatalogList { get; set; }
        public IList<SelectListItem> LevelList { get; set; }
    }
}