﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderSystem.Models.Product
{
    public class ProductModel : Domain.Model.Products.Product
    {
        public ProductModel()
        {
            StatusList = new List<SelectListItem>();
            ParCatalogList = new List<SelectListItem>();
            //LevelList = new List<SelectListItem>();
        }

        public IList<SelectListItem> StatusList { get; set; }
        public IList<SelectListItem> ParCatalogList { get; set; }
        //public IList<SelectListItem> LevelList { get; set; }
    }
}