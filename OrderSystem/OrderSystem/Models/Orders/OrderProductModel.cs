﻿using Domain.Model.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderSystem.Models.Orders
{
    public class OrderProductModel:OrderItem
    {
        public OrderProductModel()
        {
            AvailableCatalog = new List<SelectListItem>();
            AvailableProducts=new List<SelectListItem>();
        }
        public List<SelectListItem> AvailableCatalog { get; set; }
        public List<SelectListItem> AvailableProducts { get; set; }
        public int CatalogId { get; internal set; }
        public int KCCount { get; internal set; }
        public int KYCount { get; internal set; }
    }
}