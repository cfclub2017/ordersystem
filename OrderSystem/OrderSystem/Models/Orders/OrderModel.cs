﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderSystem.Models.Orders
{
    public class OrderModel : Domain.Model.Orders.Order
    {
        public OrderModel()
        {
            AvailableJXS = new List<SelectListItem>();
            AvailableFWS = new List<SelectListItem>();
            AvailableStore = new List<SelectListItem>();
            AvailableDG = new List<SelectListItem>();
            AvailableMD = new List<SelectListItem>();

        }

        public IList<SelectListItem> AvailableJXS { get; set; }
        public IList<SelectListItem> AvailableFWS { get; set; }
        public IList<SelectListItem> AvailableStore { get; set; }
        public IList<SelectListItem> AvailableDG { get; set; }
        public IList<SelectListItem> AvailableMD { get; set; }

        public string JXSName { get; set; }
        public string FWSName { get; set; }
        public string StoreName { get; set; }
        public string DGName { get; set; }
        public string MDName { get; set; }

    }
}