﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.Enums;
using Domain;

namespace OrderSystem.Extensions
{
    public static class SelectListHelper
    {
        public static SelectList ToSelectList<TEnum>(this TEnum enumObj,
           bool markCurrentAsSelected = true, int[] valuesToExclude = null, bool useLocalization = true) where TEnum : struct
        {
            if (!typeof(TEnum).IsEnum) throw new ArgumentException("An Enumeration type is required.", "enumObj");


            var values = from TEnum enumValue in System.Enum.GetValues(typeof(TEnum))
                         where valuesToExclude == null || !valuesToExclude.Contains(Convert.ToInt32(enumValue))
                         select new
                         {
                             ID = Convert.ToInt32(enumValue),
                             Name = useLocalization ? EnumExtensions.GetEnumDescription(enumValue)
                         : CommonHelper.ConvertEnum(enumValue.ToString())
                         };
            object selectedValue = null;
            if (markCurrentAsSelected)
                selectedValue = Convert.ToInt32(enumObj);
            return new SelectList(values, "ID", "Name", selectedValue);
        }

        public static List<SelectListItem> ToSelectList<T>(this IEnumerable<T> objList, Func<T, string> selector) where T : BaseEntity
        {
           return objList.Select(p => new SelectListItem { Text= selector(p), Value=p.Id.ToString() }).ToList();
            //return new SelectList(objList.Select(p => new { ID = p.Id, Name = selector(p) }), "ID", "Name");
        }
    }
}