﻿namespace OrderSystem
{
    public class ActionConfirmationModel
    {
        public int Id { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string WindowId { get; set; }
    }
}