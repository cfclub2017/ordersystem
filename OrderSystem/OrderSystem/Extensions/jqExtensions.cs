﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;

namespace OrderSystem
{
    public static class jqGridExtensions
    {

        public static HtmlString jqGrid(this HtmlHelper helper, string gridID, string caption, string url,string colNames,string colModels)
        {
            if (gridID.Substring(0, 1) != "#")
                gridID = "#" + gridID;
            string pagerID = string.Format("{0}_pager", gridID);
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(" <script type=\"text/javascript\">$(function(){");//jQuery(document).ready(function() {
            sb.AppendLine("$('%%GRIDID%%').jqGrid({".Replace("%%GRIDID%%", gridID));       //jQuery("#list").jqGrid({
            sb.AppendFormat("url:'{0}',", url);                            //        url: '/Home/GridData/',
            sb.Append("datatype: 'json',mtype: 'GET',");                 //        datatype: 'json',mtype: 'GET',
            sb.AppendFormat("colNames:[{0}],", colNames);

            sb.AppendFormat("colModel:[{0}],", colModels);
            sb.Append("autowidth: true,height: 'auto',");
            sb.Append("pager: '%%GRIDPAGERID%%',rowNum: 10,rowList: [10, 20, 50,100],".Replace("%%GRIDPAGERID%%", pagerID));
            //sb.AppendFormat("sortname:'{0}',sortorder: 'desc',", GetSortField<T>());
            sb.Append("viewrecords: true,imgpath: '/themes/redmond/images',");
            sb.AppendFormat("caption: '{0}'", caption);
            sb.Append("});\n$('%%GRIDID%%').jqGrid('navGrid','%%GRIDPAGERID%%',{ search:false,edit: false, add: false, del: false });".Replace("%%GRIDID%%", gridID).Replace("%%GRIDPAGERID%%", pagerID));
            sb.Append("});</script>\n");
            sb.AppendFormat("<table id=\"{0}\" class=\"scroll\" cellpadding=\"0\" cellspacing=\"0\"></table>", gridID.Substring(1));
            sb.AppendFormat("<div id=\"{0}\" class=\"scroll\" style=\"text-align:center;\"></div>", pagerID.Substring(1));
            sb.AppendLine();
            return new HtmlString(sb.ToString());
        }

        public static HtmlString jqTreeGrid(this HtmlHelper helper, string gridID, string caption, string url, string colNames, string colModels,string expandColumn)
        {
            if (gridID.Substring(0, 1) != "#")
                gridID = "#" + gridID;
            string pagerID = string.Format("{0}_pager", gridID);
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(" <script type=\"text/javascript\">$(function(){");//jQuery(document).ready(function() {
            sb.AppendLine("$('%%GRIDID%%').jqGrid({".Replace("%%GRIDID%%", gridID));       //jQuery("#list").jqGrid({
            sb.AppendFormat("url:'{0}',", url);                            //        url: '/Home/GridData/',
            sb.Append("datatype: 'json',mtype: 'GET',");                 //        datatype: 'json',mtype: 'GET',
            sb.AppendFormat("colNames:[{0}],", colNames);

            sb.AppendFormat("colModel:[{0}],", colModels);
            sb.Append("autowidth: true,height: 'auto',  treeGrid: true,treeGridModel: 'adjacency',ExpandColumn: '"+ expandColumn + "',");
            sb.Append(" treeReader:{level_field: \"level\", parent_id_field: \"parent\",leaf_field: \"isLeaf\",expanded_field: \"expanded\" },");
            sb.Append("pager: '%%GRIDPAGERID%%',rowNum: 10,rowList: [10, 20, 50,100],".Replace("%%GRIDPAGERID%%", pagerID));
            //sb.AppendFormat("sortname:'{0}',sortorder: 'desc',", GetSortField<T>());
            sb.Append("viewrecords: true,imgpath: '/themes/redmond/images',");
            sb.AppendFormat("caption: '{0}'", caption);
            sb.Append("});\n$('%%GRIDID%%').jqGrid('navGrid','%%GRIDPAGERID%%',{ search:false,edit: false, add: false, del: false });".Replace("%%GRIDID%%", gridID).Replace("%%GRIDPAGERID%%", pagerID));
            sb.Append("});</script>\n");
            sb.AppendFormat("<table id=\"{0}\" class=\"scroll\" cellpadding=\"0\" cellspacing=\"0\"></table>", gridID.Substring(1));
            sb.AppendFormat("<div id=\"{0}\" class=\"scroll\" style=\"text-align:center;\"></div>", pagerID.Substring(1));
            sb.AppendLine();
            return new HtmlString(sb.ToString());
        }

        public static HtmlString jqTreeGrid(this HtmlHelper helper, string gridID, string caption, string url, string colNames, string colModels, string expandColumn, string multiselect)
        {
            if (gridID.Substring(0, 1) != "#")
                gridID = "#" + gridID;
            string pagerID = string.Format("{0}_pager", gridID);
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(" <script type=\"text/javascript\">$(function(){");//jQuery(document).ready(function() {
            sb.AppendLine("$('%%GRIDID%%').jqGrid({".Replace("%%GRIDID%%", gridID));       //jQuery("#list").jqGrid({
            sb.AppendFormat("url:'{0}',", url);                            //        url: '/Home/GridData/',
            sb.Append("datatype: 'json',mtype: 'GET',");                 //        datatype: 'json',mtype: 'GET',
            sb.AppendFormat("colNames:[{0}],", colNames);

            sb.AppendFormat("colModel:[{0}],", colModels);
            sb.Append("autowidth: true,height: 'auto',  treeGrid: true,treeGridModel: 'adjacency',ExpandColumn: '" + expandColumn + "',");
            sb.Append(" treeReader:{level_field: \"level\", parent_id_field: \"parent\",leaf_field: \"isLeaf\",expanded_field: \"expanded\" },multiselect:" + multiselect + ",");
            sb.Append("pager: '%%GRIDPAGERID%%',rowNum: 10,rowList: [10, 20, 50,100],".Replace("%%GRIDPAGERID%%", pagerID));
            //sb.AppendFormat("sortname:'{0}',sortorder: 'desc',", GetSortField<T>());
            sb.Append("viewrecords: true,imgpath: '/themes/redmond/images',");
            sb.AppendFormat("caption: '{0}'", caption);
            sb.Append("});\n$('%%GRIDID%%').jqGrid('navGrid','%%GRIDPAGERID%%',{ search:false,edit: false, add: false, del: false });".Replace("%%GRIDID%%", gridID).Replace("%%GRIDPAGERID%%", pagerID));
            sb.Append("});</script>\n");
            sb.AppendFormat("<table id=\"{0}\" class=\"scroll\" cellpadding=\"0\" cellspacing=\"0\"></table>", gridID.Substring(1));
            sb.AppendFormat("<div id=\"{0}\" class=\"scroll\" style=\"text-align:center;\"></div>", pagerID.Substring(1));
            sb.AppendLine();
            return new HtmlString(sb.ToString());
        }
        public static HtmlString jqGroupGrid(this HtmlHelper helper, string gridID, string caption, string url, string colNames, string colModels,string groupName,string key,string multiselect)
        {
            if (gridID.Substring(0, 1) != "#")
                gridID = "#" + gridID;
            string pagerID = string.Format("{0}_pager", gridID);
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(" <script type=\"text/javascript\">$(function(){");//jQuery(document).ready(function() {
            sb.AppendLine("$('%%GRIDID%%').jqGrid({".Replace("%%GRIDID%%", gridID));       //jQuery("#list").jqGrid({
            sb.AppendFormat("url:'{0}',", url);                            //        url: '/Home/GridData/',
            sb.Append("datatype: 'json',mtype: 'GET',");                 //        datatype: 'json',mtype: 'GET',
            sb.AppendFormat("colNames:[{0}],", colNames);

            sb.AppendFormat("colModel:[{0}],", colModels);
            sb.Append("autowidth: true,height: 'auto',ajaxGridOptions: { async: false },");
            sb.Append("grouping: true,groupingView: {  groupField: ['"+groupName+ "']},jsonReader: { id: '"+key+ "' },multiselect:"+multiselect+",");
            sb.Append("pager: '%%GRIDPAGERID%%',rowNum: 10,rowList: [10, 20, 50,100],".Replace("%%GRIDPAGERID%%", pagerID));
            //sb.AppendFormat("sortname:'{0}',sortorder: 'desc',", GetSortField<T>());
            sb.Append("viewrecords: true,imgpath: '/themes/redmond/images',");
            sb.AppendFormat("caption: '{0}'", caption);
            sb.Append("});\n$('%%GRIDID%%').jqGrid('navGrid','%%GRIDPAGERID%%',{ search:false,edit: false, add: false, del: false });".Replace("%%GRIDID%%", gridID).Replace("%%GRIDPAGERID%%", pagerID));
            sb.Append("});</script>\n");
            sb.AppendFormat("<table id=\"{0}\" class=\"scroll\" cellpadding=\"0\" cellspacing=\"0\"></table>", gridID.Substring(1));
            sb.AppendFormat("<div id=\"{0}\" class=\"scroll\" style=\"text-align:center;\"></div>", pagerID.Substring(1));
            sb.AppendLine();
            return new HtmlString(sb.ToString());
        }

        public static HtmlString jqGroupGridAsync(this HtmlHelper helper, string gridID, string caption, string url, string colNames, string colModels, string groupName, string key, string multiselect)
        {
            if (gridID.Substring(0, 1) != "#")
                gridID = "#" + gridID;
            string pagerID = string.Format("{0}_pager", gridID);
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(" <script type=\"text/javascript\">$(function(){");//jQuery(document).ready(function() {
            sb.AppendLine("$('%%GRIDID%%').jqGrid({".Replace("%%GRIDID%%", gridID));       //jQuery("#list").jqGrid({
            sb.AppendFormat("url:'{0}',", url);                            //        url: '/Home/GridData/',
            sb.Append("datatype: 'json',mtype: 'GET',");                 //        datatype: 'json',mtype: 'GET',
            sb.AppendFormat("colNames:[{0}],", colNames);

            sb.AppendFormat("colModel:[{0}],", colModels);
            sb.Append("autowidth: true,height: 'auto',");
            sb.Append("grouping: true,groupingView: {  groupField: ['" + groupName + "']},jsonReader: { id: '" + key + "' },multiselect:" + multiselect + ",");
            sb.Append("pager: '%%GRIDPAGERID%%',rowNum: 10,rowList: [10, 20, 50,100],".Replace("%%GRIDPAGERID%%", pagerID));
            //sb.AppendFormat("sortname:'{0}',sortorder: 'desc',", GetSortField<T>());
            sb.Append("viewrecords: true,imgpath: '/themes/redmond/images',");
            sb.AppendFormat("caption: '{0}'", caption);
            sb.Append("});\n$('%%GRIDID%%').jqGrid('navGrid','%%GRIDPAGERID%%',{ search:false,edit: false, add: false, del: false });".Replace("%%GRIDID%%", gridID).Replace("%%GRIDPAGERID%%", pagerID));
            sb.Append("});</script>\n");
            sb.AppendFormat("<table id=\"{0}\" class=\"scroll\" cellpadding=\"0\" cellspacing=\"0\"></table>", gridID.Substring(1));
            sb.AppendFormat("<div id=\"{0}\" class=\"scroll\" style=\"text-align:center;\"></div>", pagerID.Substring(1));
            sb.AppendLine();
            return new HtmlString(sb.ToString());
        }

        public static HtmlString jqDialog(this HtmlHelper helper, string dialogName, int height,int width)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(" <script type=\"text/javascript\">$(function(){");//jQuery(document).ready(function() {
            sb.AppendLine("$(\"#"+ dialogName + "\").dialog({ autoOpen: false, height: "+ height + ", width: "+ width + ", modal: true });");
            sb.AppendLine("});</script>\n");

            sb.AppendLine("<div id = \""+dialogName+"\" >");
            sb.AppendLine("</div>");
            return new HtmlString(sb.ToString());
        }
    }
}