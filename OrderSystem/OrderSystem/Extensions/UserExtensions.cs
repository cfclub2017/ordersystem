﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Security.Principal;
using Microsoft.AspNet.Identity;

namespace OrderSystem.Extensions
{
    public static class UserExtensions
    {
        public static int GetCompanyId(this IIdentity identity)
        {
            return Convert.ToInt32(( (ClaimsIdentity)identity).FindFirstValue("CompanyId"));
        }

        public static int GetUserType(this IIdentity identity)
        {
            return Convert.ToInt32(((ClaimsIdentity)identity).FindFirstValue("UserType"));
        }
    }
}