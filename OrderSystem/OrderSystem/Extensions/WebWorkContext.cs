﻿using System;
using System.Linq;
using System.Web;
using Domain.Services.Common;
//using Domain.Services.Directory;
using Microsoft.AspNet.Identity;

//using Common.Fakes;
using Domain.Services;
using Common.Fakes;
using Domain.Services.Users;
using Domain.Model.Users;

namespace OrderSystem
{
    /// <summary>
    /// Work context for web application
    /// </summary>
    public partial class WebWorkContext : IWorkContext
    {


        #region Fields

        private readonly IUserService _customerService;

        private UserAccount _cachedCustomer;

        #endregion

        #region Ctor

        public WebWorkContext(
            IUserService customerService)
        {
            this._customerService = customerService;
        }

        #endregion


        #region Properties

        /// <summary>
        /// Gets or sets the current customer
        /// </summary>
        public virtual UserAccount CurrentUser
        {
            get
            {
                if (_cachedCustomer != null)
                    return _cachedCustomer;

                int userId =Convert.ToInt32 (HttpContext.Current.User.Identity.GetUserId());
                UserAccount customer = _customerService.GetUserById(userId);
                _cachedCustomer = customer;
                return _cachedCustomer;
            }
            set
            {
                _cachedCustomer = value;
            }
        }



        /// <summary>
        /// Get or set value indicating whether we're in admin area
        /// </summary>
        public virtual bool IsAdmin
        {
            get;
            set;
        }

        #endregion
    }
}
