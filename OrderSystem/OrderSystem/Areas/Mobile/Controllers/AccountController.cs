﻿using Domain.Model.Users;
using Domain.Services;
using Domain.Services.Users;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace OrderSystem.Areas.Mobile.Controllers
{
    public class AccountController : Controller
    { 
        private IWorkContext _workContext;
        private IUserService _userservice;
        public  AccountController(IWorkContext workContext,IUserService userservice)
        {
            _workContext = workContext;
            _userservice = userservice;
        }
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(Models.LoginModel login)
        {
            if (String.IsNullOrWhiteSpace(login.UserName))
            {
                ModelState.AddModelError("", "请输入用户名。");
                return View(login);
            }
            if (String.IsNullOrWhiteSpace(login.Password))
            {
                ModelState.AddModelError("", "密码不能为空。");
                return View(login);
            }
            //if (String.Compare(GetCode(), login.ValidateCode, true) != 0)
            //{
            //    ModelState.AddModelError("", "验证码错误，请输入正确的验证码。");
            //    return View(login);
            //}


           UserAccount user =_userservice.GetUserByUsername(login.UserName);

            if (user == null || user.UserPwd != login.Password.Trim())
            {
                ModelState.AddModelError("", "用户名或密码错误。");
                return View(login);
            }
            //if (user.UserType != (int)Domain.Model.EUserType.SYSTEMADMIN && user.UserType != (int)Domain.Model.EUserType.COMPANYADMIN)
            //{
            //    ModelState.AddModelError("", "您无权登录当前系统。");
            //    return View(login);
            //}


            ClaimsIdentity _identity = new ClaimsIdentity(CookicNames.AdminCookie);
            //_identity.AddClaim(new Claim("UserType", user.UserType.ToString()));
            _identity.AddClaim(new Claim("CompanyId", user.CompanyID.ToString()));
            _identity.AddClaim(new Claim(ClaimTypes.Name, user.Name.ToString()));
            _identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
            //_identity.AddClaim(new Claim("DisplayName", user.DisplayName));
            HttpContext.GetOwinContext().Authentication.SignIn(new AuthenticationProperties() { IsPersistent = login.RememberMe }, _identity);
            _workContext.CurrentUser = user;
            ////定义身份验证 Cookie
            //IAuthenticationService authenticationService = ObjectContainer.Resolve<IAuthenticationService>();
            //authenticationService.SignIn(user, login.RememberMe);
            return RedirectToAction("Index", "Home");
        }
        //[AllowAnonymous]
        //public ActionResult GetImageCode()
        //{
        //    ValidateCode vCode = new ValidateCode();
        //    string code = vCode.CreateValidateCode(4);
        //    Session["ValidateCode"] = code;
        //    byte[] bytes = vCode.CreateValidateGraphic(code);
        //    return File(bytes, @"image/jpeg");
        //}
        //public string GetCode()
        //{
        //    return Session["ValidateCode"].ToString();
        //}

        public ActionResult LogOut()
        {
            HttpContext.GetOwinContext().Authentication.SignOut(CookicNames.AdminCookie);
            _workContext.CurrentUser = null;
            return RedirectToAction("Login", "Account");
        }

        public ActionResult UpdMyPwd()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UpdMyPwd(string txtOldPwd, string txtNewPwd, string txtCNewPwd)
        {
            //if (txtCNewPwd != txtNewPwd)
            //    throw new Exception("两次密码输入不匹配！");


            //string tmp = Common.Security.Encryption.EncryptMd5Text(txtOldPwd.Trim());
            //if (user.UserPwd != tmp)
            //    throw new Exception("原始密码错误！");

            //var userService = new UserService();
            //userService.UpdMyUserPwd(user, txtNewPwd);
            return Content("ok");
        }
    }
}