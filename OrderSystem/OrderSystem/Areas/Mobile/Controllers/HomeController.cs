﻿using Common.Enums;
using Domain.Model.Orders;
using Domain.Model.Users;
using Domain.Services;
using Domain.Services.Companys;
using Domain.Services.Orders;
using Domain.Services.Stores;
using Domain.Services.Users;
using Microsoft.Owin.Security;
using OrderSystem.Areas.Mobile.Views.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace OrderSystem.Areas.Mobile.Controllers
{
    public class HomeController : Controller
    {
        private IWorkContext _workContext;
        private IOrderService _orderService;
        private ISubordinateCompanyService _subordinateCompany;
        private IUserService _userService;
        private IStoreService _store;
        //private ICatalogService _catalogService;
        //private IProductService _productService;
        public HomeController(
            //IProductService productService,
            //ICatalogService catalogService,
            IStoreService store,
            IUserService userService,
            ISubordinateCompanyService subordinateCompany,
            IOrderService orderService,
            IWorkContext workContext)
        {
            //_productService = productService;
            //_catalogService = catalogService;
            _store = store;
            _userService = userService;
            _subordinateCompany = subordinateCompany;
            _orderService = orderService;
            _workContext = workContext;
           
        }

        public ActionResult Login(Models.LoginModel login)
        {
            if (String.IsNullOrWhiteSpace(login.UserName))
            {
                ModelState.AddModelError("", "请输入用户名。");
                return View(login);
            }
            if (String.IsNullOrWhiteSpace(login.Password))
            {
                ModelState.AddModelError("", "密码不能为空。");
                return View(login);
            }
            //if (String.Compare(GetCode(), login.ValidateCode, true) != 0)
            //{
            //    ModelState.AddModelError("", "验证码错误，请输入正确的验证码。");
            //    return View(login);
            //}


            UserAccount user = _userService.GetUserByUsername(login.UserName);

            if (user == null || user.UserPwd != login.Password.Trim())
            {
                ModelState.AddModelError("", "用户名或密码错误。");
                return View(login);
            }
            //if (user.UserType != (int)Domain.Model.EUserType.SYSTEMADMIN && user.UserType != (int)Domain.Model.EUserType.COMPANYADMIN)
            //{
            //    ModelState.AddModelError("", "您无权登录当前系统。");
            //    return View(login);
            //}


            ClaimsIdentity _identity = new ClaimsIdentity(CookicNames.AdminCookie);
            //_identity.AddClaim(new Claim("UserType", user.UserType.ToString()));
            _identity.AddClaim(new Claim("CompanyId", user.CompanyID.ToString()));
            _identity.AddClaim(new Claim(ClaimTypes.Name, user.Name.ToString()));
            _identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
            //_identity.AddClaim(new Claim("DisplayName", user.DisplayName));
            _workContext.CurrentUser = user;
            ////定义身份验证 Cookie
            //IAuthenticationService authenticationService = ObjectContainer.Resolve<IAuthenticationService>();
            //authenticationService.SignIn(user, login.RememberMe);
            return RedirectToAction("Index", "Home");
        }
        // GET: Mobile/Home
        public ActionResult Index()
        {
            Login(new Models.LoginModel
            {
                RememberMe = true,
                UserName = "md",
                Password = "123456"
            });

            return View();
            //int pageSize = 10;// Convert.ToInt32(Request.QueryString["rows"]);
            //int pageIndex = 0;// Convert.ToInt32(Request.QueryString["page"]) - 1;
            //var data = _orderService.GetOrderList(_workContext.CurrentUser, pageIndex, pageSize);
            //List<OrderListModel> model = data.Select(p => new OrderListModel
            //{
            //    Id = p.Id,
            //    OrderNum = p.OrderNum,
            //    StatusName = EnumExtensions.GetEnumDescription((OrderStatus)p.Status),
            //    OrderTotal = p.OrderItems.Sum(c =>c.Quantity* c.Price),
            //    Quantity=p.OrderItems.Sum(c=>c.Quantity),
            //    //StoreName = p.Store == null ? "" : p.Store.Name,
            //    ////WLSName = p.w
            //    //JXSName = p.JXS == null ? "" : p.JXS.Name,
            //    MDName = p.MD == null ? "" : p.MD.Name,
            //    //DGName = p.DG == null ? "" : p.DG.Name,
            //    //FWSName = p.FWS == null ? "" : p.FWS.Name,
            //    CustomerName = p.CustomerName,
            //    CustomerTel1 = p.CustomerTel1,// + "/" + p.CustomerTel2,
            //    Province = p.Province,
            //    City = p.City,
            //    County = p.County,
            //    Address = p.Address,
            //    CreateTime = p.CreateTime,
            //    Remarks = p.Remarks
            //    //,
            //    //CheckTime = p.JSXCheckTime,
            //    //DeliverTime = p.WLDeliverTime
            //}).ToList();
            ////return model.ToJQGridJSON(data.PageIndex, data.PageSize, data.TotalCount);
            //return View(model);
        }
    }
}