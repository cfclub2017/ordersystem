﻿using Domain.Services;
using Domain.Services.Products;
using Domain.Services.Stores;
using OrderSystem.Areas.Mobile.Views.Models;
using OrderSystem.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderSystem.Areas.Mobile.Controllers
{
    public class StorageController : Controller
    {
        private IWorkContext _workContext;

        //private IUserService _userService;
        private IStoreService _store;
        private ICatalogService _catalogService;
        private IProductService _productService;
        public StorageController(
            IProductService productService,
            ICatalogService catalogService,
            IStoreService store,
            //IUserService userService,
            //IPictureService pictureService,
            //ISubordinateCompanyService subordinateCompany,
            //IOrderService orderService,
            IWorkContext workContext)
        {
            _productService = productService;
            _catalogService = catalogService;
            _store = store;
            //_userService = userService;
            //_pictureService = pictureService;
            //_subordinateCompany = subordinateCompany;
            //_orderService = orderService;
            _workContext = workContext;
        }
        // GET: Mobile/Storage
        public ActionResult ProductStore()
        {
            var catalogs = _catalogService.GetAllCataLog(_workContext.CurrentUser);
            ViewData["Catalog"] = catalogs.ToSelectList(p => p.Name);
            ViewData["Store"] = _store.GetStore(_workContext.CurrentUser).ToSelectList(p => p.Name);

            return View();
        }

        public ActionResult GetAllStorage(int? storeId,int? catalogId,string productName,int pageIndex)
        {
            int pageSize = 10;// Convert.ToInt32(Request.QueryString["rows"]);
            var data = _productService.getAllProductStore(_workContext.CurrentUser,productName,storeId==null?-1:(int)storeId,catalogId==null?-1:(int)catalogId, pageIndex, pageSize);
            List<ProductStoreListModel> model = data.Select(p => new ProductStoreListModel
            {
                ActualQuantity = p.ActualQuantity,
                Num = p.Product.Num,
                ProductName = p.Product.Name,
                Quantity = p.Quantity

            }).ToList();

            PagedResultModel<ProductStoreListModel> list = new PagedResultModel<ProductStoreListModel>(data.TotalCount, data.TotalPages, data.PageSize, data.PageIndex, model);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}