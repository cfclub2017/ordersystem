﻿using Common;
using Domain.Services;
using Domain.Services.Orders;
using OrderSystem.Areas.Mobile.Views.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderSystem.Areas.Mobile.Controllers
{
    public class SalesController : Controller
    {
        private IWorkContext _workContext;
        private IOrderService _orderService;
        //private ISubordinateCompanyService _subordinateCompany;
        //private IUserService _userService;
        //private IStoreService _store;
        //private ICatalogService _catalogService;
        //private IProductService _productService;
        public SalesController(
            //IProductService productService,
            //ICatalogService catalogService,
            //IStoreService store,
            //IUserService userService,
            //ISubordinateCompanyService subordinateCompany,
            IOrderService orderService,
            IWorkContext workContext)
        {
            //_productService = productService;
            //_catalogService = catalogService;
            //_store = store;
            //_userService = userService;
            //_subordinateCompany = subordinateCompany;
            _orderService = orderService;
            _workContext = workContext;

        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetDayOrders(int pageIndex,DateTime? startTime, DateTime? endTime,string status)
        {
            int pageSize = 10;// Convert.ToInt32(Request.QueryString["rows"]);
            var data = _orderService.GetDayOrders(_workContext.CurrentUser,startTime,endTime,status, pageIndex, pageSize);
            PagedResultModel<object> list = new PagedResultModel<object>(data.TotalCount,data.TotalPages,data.PageSize,data.PageIndex, data);
            return Json(list,  JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMonthOrders(int pageIndex, DateTime? startTime, DateTime? endTime, string status)
        {
            int pageSize = 10;// Convert.ToInt32(Request.QueryString["rows"]);
            var data = _orderService.GetMonthOrders(_workContext.CurrentUser, startTime, endTime, status, pageIndex, pageSize);
            PagedResultModel<object> list = new PagedResultModel<object>(data.TotalCount, data.TotalPages, data.PageSize, data.PageIndex, data);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }

}