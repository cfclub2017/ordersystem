﻿using Common.Enums;
using Domain.Model.Comapnys;
using Domain.Model.Orders;
using Domain.Model.Products;
using Domain.Services;
using Domain.Services.Companys;
using Domain.Services.Media;
using Domain.Services.Orders;
using Domain.Services.Products;
using Domain.Services.Users;
using OrderSystem.Areas.Mobile.Views.Models;
using OrderSystem.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderSystem.Areas.Mobile.Controllers
{
    public class OrderController : Controller
    {
        private IWorkContext _workContext;
        private IOrderService _orderService;
        private ISubordinateCompanyService _subordinateCompany;
        private IPictureService _pictureService;
        private IUserService _userService;
        //private IStoreService _store;
        private ICatalogService _catalogService;
        private IProductService _productService;
        public OrderController(
            IProductService productService,
            ICatalogService catalogService,
            //IStoreService store,
            IUserService userService,
            IPictureService pictureService,
            ISubordinateCompanyService subordinateCompany,
            IOrderService orderService,
            IWorkContext workContext)
        {
            _productService = productService;
            _catalogService = catalogService;
            //_store = store;
            _userService = userService;
            _pictureService = pictureService;
            _subordinateCompany = subordinateCompany;
            _orderService = orderService;
            _workContext = workContext;
        }

        #region 公用方法
        private Order ToOrder(OrderModel model)
        {
            string[] area = model.AreaAddress.Split(' ');
            return new Order
            {
                Address = model.Address,
                AgentId = model.AgentId,
                AreaCheckTime = model.AreaCheckTime,
                AreaId = model.AreaId,
                CancelTime = model.CancelTime,
                City = area.Length>1?area[1]:"",
                County = area.Length > 2 ? area[2] : "",
                CreateTime = DateTime.Now,
                CreateUserId = _workContext.CurrentUser.Id,
                CustomerName = model.CustomerName,
                CustomerTel1 = model.CustomerTel1,
                CustomerTel2 = model.CustomerTel2,
                DGId = model.DGId,
                DGSubmitTime = model.DGSubmitTime,
                FWSID = model.FWSID,
                FWSSubmitStatus = model.FWSSubmitStatus,
                FWSSubmitTime = model.FWSSubmitTime,
                FWSUserCompleteTime = model.FWSUserCompleteTime,
                FWSUserName = model.FWSUserName,
                FWSUserSubmitStatus = model.FWSUserSubmitStatus,
                FWSUserSubmitTime = model.FWSUserSubmitTime,
                FWSUserTel = model.FWSUserTel,
                Id = model.Id,
                JSXCheckTime = model.JSXCheckTime,
                JXSId = model.JXSId,
                MDId = model.MDId,
                OrderNum = model.OrderNum,
                Province = area.Length > 0 ? area[0] : "",
                Remarks = model.Remarks,
                RequireDeliveryTime = model.RequireDeliveryTime,
                RequireInstallTime = model.RequireInstallTime,
                Status = model.Status,
                StoreId = model.StoreId,
                WLCheckTime = model.WLCheckTime,
                WLDeliverTime = model.WLDeliverTime
            };
        }
        #endregion

        public ActionResult AddOrderMain()
        {
            return View();
        }

        public ActionResult AddOrder()
        {
            var user = _userService.GetUserById(_workContext.CurrentUser.Id);
            var model = new OrderModel()
            {
                 MDName= user.SubordinateCompany.Name,
                //AvailableDG = _userService.GetUserByType(_workContext.CurrentUser, UserType.DG).ToSelectList(p => p.Name),
                AvailableFWS = _subordinateCompany.GetSubCompanyByType(_workContext.CurrentUser, SubordinateComapnyType.FWS).ToSelectList(p => p.Name),
                //AvailableJXS = _subordinateCompany.GetSubCompanyByType(_workContext.CurrentUser, SubordinateComapnyType.JXS).ToSelectList(p => p.Name),
                //AvailableMD = _subordinateCompany.GetSubCompanyByType(_workContext.CurrentUser, SubordinateComapnyType.MD).ToSelectList(p => p.Name),
                //AvailableStore = _store.GetStore(_workContext.CurrentUser).ToSelectList(p => p.Name)
                 AvailableArea= _subordinateCompany.GetSubCompanyByType(_workContext.CurrentUser, SubordinateComapnyType.Area).ToSelectList(p => p.Name)
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult AddOrder(OrderModel model,string[] productQuantity)
        {
            var user = _userService.GetUserById(_workContext.CurrentUser.Id);
            var order = ToOrder(model);
            order.Status = (int)OrderStatus.Submit;
            order.OrderNum = DateTime.Now.ToString("yyyyMMddHHmmSSffff");
            order.MDId = user.SubordinateCompany.Id;
            order.JXSId = user.SubordinateCompany.ParentId;
            order.DGId = user.Id;
            HttpFileCollectionBase files = Request.Files;//这里只能用<input type="file" />才能有效果,因为服务器控件是HttpInputFile类型
            foreach (string filekey in files)
            {
                HttpPostedFileBase file = files[filekey];
                //var fileName = Path.Combine(Request.MapPath("~/Upload"), Path.GetFileName(file.FileName));
                try
                {
                    Stream stream = file.InputStream;
                    string contentType = file.ContentType;
                    var fileBinary = new byte[stream.Length];
                    stream.Read(fileBinary, 0, fileBinary.Length);
                    var picture = _pictureService.InsertPicture(fileBinary, contentType, "");
                    order.OrderPictures.Add(picture);
                }
                catch
                {
                    return Content("上传异常 ！", "text/plain");
                }
            }
            if (productQuantity != null)
            {
                foreach (string prodcutq in productQuantity)
                {
                    int[] pq = prodcutq.Split('_').Select(p => Convert.ToInt32(p)).ToArray();
                    var product= _productService.getProductById(pq[0]);
                    order.OrderItems.Add(new OrderItem { ProductId = pq[0], Price= product.SuggestPrice,  Quantity = pq[1] });
                }
            }
            _orderService.AddOrder(order);
            return RedirectToAction("Index","Home");
        }

        public ActionResult AddOrderProducts()
        {
            var catalogs = _catalogService.GetAllCataLog(_workContext.CurrentUser).Where(p=>p.Level==2).ToList();
            ViewData["Catalog"] = catalogs.ToSelectList(p => p.Name);
            var products = catalogs.Count <= 0 ? new List<Product>() : _productService.getProductByCatalog(catalogs.FirstOrDefault().Id);
            return View(products);
        }

        public ActionResult GetAllOrders(int pageIndex, string orderNum, string status)
        {
            int pageSize = 10;// Convert.ToInt32(Request.QueryString["rows"]);
            var data = _orderService.GetAllOrders(_workContext.CurrentUser, orderNum, status, pageIndex, pageSize);
         
            List<OrderListModel> model = data.Select(p => new OrderListModel
            {
                Id = p.Id,
                OrderNum = p.OrderNum,
                StatusName = EnumExtensions.GetEnumDescription((OrderStatus)p.Status),
                OrderTotal = p.OrderItems.Sum(c => c.Quantity * c.Price),
                Quantity = p.OrderItems.Sum(c => c.Quantity),
                MDName = p.MD == null ? "" : p.MD.Name,
                CustomerName = p.CustomerName,
                CustomerTel1 = p.CustomerTel1,// + "/" + p.CustomerTel2,
                Province = p.Province,
                City = p.City,
                County = p.County,
                Address = p.Address,
                CreateTimeFormat = p.CreateTime.ToString("yyyy-MM-dd HH:mm ddd"),
                Remarks = p.Remarks
            }).ToList();

            PagedResultModel<OrderListModel> list = new PagedResultModel<OrderListModel>(data.TotalCount, data.TotalPages, data.PageSize, data.PageIndex, model);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}