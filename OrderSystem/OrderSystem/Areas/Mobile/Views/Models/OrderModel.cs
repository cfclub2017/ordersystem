﻿using Domain.Model.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderSystem.Areas.Mobile.Views.Models
{
    public class OrderModel : Order
    {
        public OrderModel()
        {
            //AvailableMD = new List<SelectListItem>();
            AvailableArea = new List<SelectListItem>();
            AvailableFWS = new List<SelectListItem>();
        }

        public string AreaAddress { get; set; }
        public string MDName { get; set; }
        //public IList<SelectListItem> AvailableMD { get; set; }
        public IList<SelectListItem> AvailableArea { get; set; }
        public List<SelectListItem> AvailableFWS { get; internal set; }
    }
}