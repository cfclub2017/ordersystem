﻿using Domain.Model.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderSystem.Areas.Mobile.Views.Models
{
    public class OrderListModel:Order
    {
        public string CreateTimeFormat { get; set; }
        public string StatusName { get; set; }
        /// <summary>
        /// 门店名称
        /// </summary>
        public string MDName { get; set; }
        /// <summary>
        /// 订单总额
        /// </summary>
        public decimal OrderTotal { get; set; }

        public int Quantity { get; set; }
    }
}