﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderSystem.Areas.Mobile.Views.Models
{
    public class ProductStoreListModel
    {
        public string ProductName { get; set; }
        public string Num { get; set; }
        public int ActualQuantity { get; set; }
        public int Quantity { get; set; }
    }
}