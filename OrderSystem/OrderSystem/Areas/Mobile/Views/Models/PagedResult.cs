﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace OrderSystem.Areas.Mobile.Views.Models
{
    /// <summary>
    /// 表示包含了分页信息的集合类型。
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PagedResultModel<T>
    {
        #region Public Fields
        /// <summary>
        /// 获取一个当前类型的空值。
        /// </summary>
        public static readonly PagedResultModel<T> Empty = new PagedResultModel<T>(0, 0, 0, 0, null);
        #endregion

        #region Ctor
        /// <summary>
        /// 初始化一个新的<c>PagedResult{T}</c>类型的实例。
        /// </summary>
        public PagedResultModel()
        {
            Data = new List<T>();
        }
        /// <summary>
        /// 初始化一个新的<c>PagedResult{T}</c>类型的实例。
        /// </summary>
        /// <param name="totalRecords">总记录数。</param>
        /// <param name="totalPages">页数。</param>
        /// <param name="pageSize">页面大小。</param>
        /// <param name="pageNumber">页码。</param>
        /// <param name="data">当前页面的数据。</param>
        public PagedResultModel(int totalRecords, int totalPages, int pageSize, int pageNumber, IList<T> data)
        {
            this.TotalPages = totalPages;
            this.TotalRecords = totalRecords;
            this.PageSize = pageSize;
            this.PageNumber = pageNumber;
            this.Data = data;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// 获取或设置总记录数。
        /// </summary>
        public int TotalRecords { get; set; }
        /// <summary>
        /// 获取或设置页数。
        /// </summary>
        public int TotalPages { get; set; }
        /// <summary>
        /// 获取或设置页面大小。
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// 获取或设置页码。
        /// </summary>
        public int PageNumber { get; set; }
        /// <summary>
        /// 获取或设置当前页面的数据。
        /// </summary>
        public IList<T> Data { get; set; }
        #endregion

    }
}
