﻿(function ($) {
    $.fn.loadMore = function (url,fun) {
        
        var trackLoad = { url:url, pageIndex: 0,pageCount:10, loading: false,resultFun:fun };//第几页
      
        //鼠标滚动到底显示更多
        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                if (trackLoad.pageIndex < trackLoad.pageCount && !trackLoad.loading) {
                    trackLoad.loading = true;
                    $(".animation_image").show();
                    $.GetData(trackLoad);
                }
            }
        });
        //一进来就加载第一页
        $.GetData(trackLoad);
    };

    ////设置对象loadMore的默认值
    //$.fn.loadMore.default = {
    //    url: "/Mobile/Sales/test",
    //    pageIndex: 1,
    //    loading: false
    //};

    $.GetData = function (options) {
        var opts = $.extend(true, {}, $.fn.loadMore.default, options);

        $.ajax({
            url: opts.url,
            type: "POST",
            dataType: "JSON",
            data: { pageIndex: opts.pageIndex },
            success: function (data) {
                if (data != null && data != undefined && data != "") {
                    var obj = eval(data);
                    options.resultFun(obj.Data);
                    
                    $(".animation_image").hide();
                    options.pageIndex++;
                    options.loading = false;
                    options.pageCount = obj.TotalPages;
                }
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                options.loading = true;
                console.log(errorThrown.toString());
            }
        });

    };
    //$.fn.loadMore();
})(jQuery);