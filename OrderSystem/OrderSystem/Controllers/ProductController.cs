﻿using Common;
using Common.Enums;
using Domain.Model.Comapnys;
using Domain.Model.Common;
using Domain.Model.Products;
using Domain.Services;
using Domain.Services.Companys;
using Domain.Services.Products;
using OrderSystem.Extensions;
using OrderSystem.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderSystem.Controllers
{
    public class ProductController : Controller
    {
        private IWorkContext _workContext;
        private IProductService _productService;
        private ICatalogService _catalogService;
        private ISubordinateCompanyService _subordinateCompanyService;
        public ProductController(IProductService productService, IWorkContext workContext,
            ICatalogService catalogService,ISubordinateCompanyService subordinateCompany)
        {
            _productService = productService;
            _workContext = workContext;
            _catalogService = catalogService;
            _subordinateCompanyService = subordinateCompany;
        }
        // GET: Product
        public ActionResult getProductByCatalog(int Id)
        {
            var model = _productService.getProductByCatalog(Id);
            return Json(model.ToSelectList(p => p.Name), JsonRequestBehavior.AllowGet);
        }
        public ActionResult getProductById(int Id)
        {
            var model = _productService.getProductById(Id);
            int kccount = model.ProductStores.Where(p => p.SubordinateCompanyId == _workContext.CurrentUser.SubordinateCompanyId).FirstOrDefault().Quantity;
            return Json(new
            {
                KYCount = kccount,
                KCCount = kccount
            }, JsonRequestBehavior.AllowGet);
        }

        #region 商品管理
        public ActionResult ProductList()
        {
            IList<SelectListItem> productStatusList = EnumExtensions.GetSelectList<ProductStatus>();
            Catalog parm = new Catalog();
            parm.Level = 1;
            List<Catalog> one = _catalogService.GetAllCataLogByParm(parm);
            //parm.Level = 2;
            //List<Catalog> two = _catalogService.GetAllCataLogByParm(parm);
            ViewData["productStatusList"] = productStatusList;
            ViewData["one"] = one;
            //ViewData["two"] = two;
            return View();
        }

        public string GetProductList()
        {
            int pageSize = Convert.ToInt32(Request.QueryString["rows"]);
            int pageIndex = Convert.ToInt32(Request.QueryString["page"]) - 1;
            Product parms = new Product();
            parms.Name = Request.QueryString["Name"];
            parms.Num = Request.QueryString["Num"];
            parms.Status = Convert.ToInt32(Request.QueryString["Status"]);
            parms.Specification = Request.QueryString["Specification"];
            if(string.IsNullOrEmpty(Request.QueryString["ParentCatalogId"]))
                parms.ParentCataloglogId = Convert.ToInt32(Request.QueryString["ParentCatalogId"]);
            if (string.IsNullOrEmpty(Request.QueryString["CatalogId"]))
                parms.CatalogId = Convert.ToInt32(Request.QueryString["CatalogId"]);
            var data = _productService.GetProductList(parms, pageIndex, pageSize);

            List<object> model = data.Select(p => (object)new
            {
                Id = p.Id,
                Name = p.Name,
                Num = p.Num,
                Specification = p.Specification,
                Cover = p.Cover,
                CatalogId = p.CatalogId,
                //CatalogOne = p.Catalog.Parent.Name,
                CatalogTwo = p.Catalog.Name,
                Remarks = p.Remarks,
                SuggestPrice = p.SuggestPrice,
                IsInstall = p.IsInstall,
                Status = EnumExtensions.GetEnumDescription((ProductStatus)p.Status)
            }).ToList();
            return model.ToJQGridJSON(data.PageIndex, data.PageSize, data.TotalCount);
        }

        public ActionResult AddProduct()
        {
            IList<SelectListItem> productStatusList = EnumExtensions.GetSelectList<ProductStatus>();
            Catalog parm = new Catalog();
            parm.Level = 1;
            List<Catalog> one = _catalogService.GetAllCataLogByParm(parm);

            ViewData["productStatusList"] = productStatusList;
            ViewData["one"] = one;

            var model = new ProductModel();
            model.StatusList = productStatusList;
            model.ParCatalogList = _catalogService.GetCataLogList(new Catalog(), 0, 100).ToSelectList(p => p.Name);
            return View(model);
        }

        [HttpPost]
        public ActionResult AddProduct(Product model)
        {

            //model.CreateUser = _workContext.CurrentUser;
            model.UserId = _workContext.CurrentUser.Id;
            //if (model.IsInstall)
            //    model.IsInstall = true;
            //else
            //    model.IsInstall = false;
            _productService.Add(model);
            return Json(new
            {
                success = true,
                message = "添加成功"
            });


        }

        [HttpPost]
        public ActionResult DelProduct(int id)
        {
            _productService.Del(id);
            return Json(new
            {
                success = true,
                message = "删除成功"
            });
        }

        public ActionResult UpdProduct(int id)
        {
            Catalog parm = new Catalog();
            parm.Level = 1;
            List<Catalog> one = _catalogService.GetAllCataLogByParm(parm);

            ViewData["one"] = one;

            Product product = _productService.GetById(id);
            ProductModel model = new ProductModel();
            model.Name = product.Name;
            model.Num = product.Num;
            model.Img = product.Img;
            model.CostPrice = product.CostPrice;
            model.CatalogId = product.CatalogId;
            model.Catalog = product.Catalog;
            //model.ParentCatalog = product.Catalog.Parent;
            model.Remarks = product.Remarks;
            model.Specification = product.Specification;
            model.SuggestPrice = product.SuggestPrice;
            model.MaxPrice = product.MaxPrice;
            model.MinPrice = product.MinPrice;
            model.CRMNum = product.CRMNum;
            model.UserId = product.UserId;
            model.Cover = product.Cover;
            model.Status = product.Status;
            model.Id = product.Id;
            model.ParCatalogList = _catalogService.GetCataLogList(new Catalog(), 0, 100).ToSelectList(p => p.Name);
            IList<SelectListItem> items = EnumExtensions.GetSelectList<ProductStatus>();
            foreach (SelectListItem item in items)
            {
                if (item.Value == Convert.ToString(model.Status))
                {
                    item.Selected = true;
                }
            }
            model.StatusList = items;
            return View(model);
        }

        [HttpPost]
        public ActionResult UpdProduct(Product user)
        {
            _productService.Upd(user);
            return Json(new
            {
                success = true,
                message = "修改成功"
            });
        }

        #endregion

        #region 品类管理
        public ActionResult CatalogList()
        {
            return View();
        }

        //public FileContentResult GetCatalogImage(int id)
        //{
        //    Catalog model = _catalogService.GetById(id);
        //    if (model != null)
        //    {
        //        return File(model.Icon, model.ImageMimeType);
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}

        public string GetCatalogList()
        {
            int pageSize = Convert.ToInt32(Request.QueryString["rows"]);
            int pageIndex = Convert.ToInt32(Request.QueryString["page"]) - 1;
            Catalog parms = new Catalog();
            parms.Name = Request.QueryString["Name"];
            parms.ParentId = Convert.ToInt32(Request.QueryString["ParentId"]);
            parms.Level = Convert.ToInt32(Request.QueryString["Level"]);
            var data = _catalogService.GetCataLogList(parms, pageIndex, pageSize);

            List<object> model = data.Select(p => (object)new
            {
                Id = p.Id,
                Name = p.Name,
                Num = p.Num,
                ParentId = p.ParentId,
                ParentName = p.ParentName,
                CompanyName = p.CompanyName,
                Remarks = p.Remarks,
                Level = EnumExtensions.GetEnumDescription((Level)p.Level),
                Icon = p.Icon,
                ImageMimeType = p.ImageMimeType
             }).ToList();
            return model.ToJQGridJSON(data.PageIndex, data.PageSize, data.TotalCount);
        }

        public ActionResult AddCatalog()
        {
            var model = new CatalogModel();

            model.CompanyList =
                _subordinateCompanyService.GetSubCompanyByType(_workContext.CurrentUser, SubordinateComapnyType.ALL).ToSelectList(p => p.Name);
            model.ParCatalogList = _catalogService.GetCataLogList(new Catalog(), 0, 100).ToSelectList(p => p.Name);
            model.LevelList = EnumExtensions.GetSelectList<Level>();
            return View(model);
        }

        [HttpPost]
        public ActionResult AddCatalog(Catalog model,HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                 _catalogService.Add(model);
                return Json(new
                {
                    success = true,
                    message = "添加成功"
                });
            }
            else
            {
                return Json(new
                {
                    success = true,
                    message = "添加失败"
                });
            }          
            
        }

        [HttpPost]
        public ActionResult DelCatalog(int id)
        {
            _catalogService.Del(id);
            return Json(new
            {
                success = true,
                message = "删除成功"
            });
        }

        public ActionResult UpdCatalog(int id)
        {
            var catalog = _catalogService.GetById(id);
            CatalogModel model = new CatalogModel();
            model.Id = catalog.Id;
            model.Name = catalog.Name;
            model.Num = catalog.Num;
            model.ParentName = catalog.ParentName;
            model.CompanyName = catalog.CompanyName;
            model.Remarks = catalog.Remarks;
            model.Icon = catalog.Icon;
            model.Level = catalog.Level;
            model.ParentId = catalog.ParentId;
            model.CompanyId = catalog.CompanyId;

            List<SelectListItem> items = _subordinateCompanyService.GetSubCompanyByType(_workContext.CurrentUser, SubordinateComapnyType.ALL).ToSelectList(p => p.Name);
            foreach (SelectListItem item in items)
            {
                if (item.Value == Convert.ToString(catalog.CompanyId))
                {
                    item.Selected = true;
                }
            }
            model.CompanyList = items;


            items = _catalogService.GetCataLogList(new Catalog(), 0, 100).ToSelectList(p => p.Name);
            foreach (SelectListItem item in items)
            {
                if (item.Value == Convert.ToString(catalog.ParentId))
                {
                    item.Selected = true;
                }
            }
            model.ParCatalogList = items;

            items = EnumExtensions.GetSelectList<Level>();
            foreach (SelectListItem item in items)
            {
                if (item.Value == Convert.ToString(catalog.Level))
                {
                    item.Selected = true;
                }
            }
            model.LevelList = items;
            return View(model);
        }

        [HttpPost]
        public ActionResult UpdCatalog(Catalog user)
        {
            _catalogService.Upd(user);
            return Json(new
            {
                success = true,
                message = "修改成功"
            });
        }
        #endregion
    }
}