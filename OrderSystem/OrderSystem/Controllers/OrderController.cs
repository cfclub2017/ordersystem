﻿using Common;
using Common.Enums;
using Domain.Model.Comapnys;
using Domain.Model.Orders;
using Domain.Model.Users;
using Domain.Services;
using Domain.Services.Companys;
using Domain.Services.Orders;
using Domain.Services.Products;
using Domain.Services.Stores;
using Domain.Services.Users;
using OrderSystem.Extensions;
using OrderSystem.Models.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderSystem.Controllers
{
    public class OrderController : Controller
    {
        private IWorkContext _workContext;
        private IOrderService _orderService;
        private ISubordinateCompanyService _subordinateCompany;
        private IUserService _userService;
        private IStoreService _store;
        private ICatalogService _catalogService;
        private IProductService _productService;
        public OrderController(
            IProductService productService,
            ICatalogService catalogService,
            IStoreService store,
            IUserService userService,
            ISubordinateCompanyService subordinateCompany,
            IOrderService orderService,
            IWorkContext workContext)
        {
            _productService = productService;
            _catalogService = catalogService;
            _store = store;
            _userService = userService;
            _subordinateCompany = subordinateCompany;
            _orderService = orderService;
            _workContext = workContext;
        }

        #region 公用方法
        private Order ToOrder(OrderModel model)
        {
            return new Order
            {
                Address = model.Address,
                AgentId = model.AgentId,
                AreaCheckTime = model.AreaCheckTime,
                AreaId = model.AreaId,
                CancelTime = model.CancelTime,
                City = model.City,
                County = model.County,
                CreateTime = DateTime.Now,
                CreateUserId = _workContext.CurrentUser.Id,
                CustomerName = model.CustomerName,
                CustomerTel1 = model.CustomerTel1,
                CustomerTel2 = model.CustomerTel2,
                DGId = model.DGId,
                DGSubmitTime = model.DGSubmitTime,
                FWSID = model.FWSID,
                FWSSubmitStatus = model.FWSSubmitStatus,
                FWSSubmitTime = model.FWSSubmitTime,
                FWSUserCompleteTime = model.FWSUserCompleteTime,
                FWSUserName = model.FWSUserName,
                FWSUserSubmitStatus = model.FWSUserSubmitStatus,
                FWSUserSubmitTime = model.FWSUserSubmitTime,
                FWSUserTel = model.FWSUserTel,
                Id = model.Id,
                JSXCheckTime = model.JSXCheckTime,
                JXSId = model.JXSId,
                MDId = model.MDId,
                OrderNum = model.OrderNum,
                Province = model.Province,
                Remarks = model.Remarks,
                RequireDeliveryTime = model.RequireDeliveryTime,
                RequireInstallTime = model.RequireInstallTime,
                Status = model.Status,
                StoreId = model.StoreId,
                WLCheckTime = model.WLCheckTime,
                WLDeliverTime = model.WLDeliverTime
            };
        }

        public OrderModel ToModel(Order model)
        {
            return new OrderModel
            {
                Address = model.Address,
                AgentId = model.AgentId,
                AreaCheckTime = model.AreaCheckTime,
                AreaId = model.AreaId,
                CancelTime = model.CancelTime,
                City = model.City,
                County = model.County,
                CreateTime = DateTime.Now,
                CreateUserId = _workContext.CurrentUser.Id,
                CustomerName = model.CustomerName,
                CustomerTel1 = model.CustomerTel1,
                CustomerTel2 = model.CustomerTel2,
                DGId = model.DGId,
                DGSubmitTime = model.DGSubmitTime,
                FWSID = model.FWSID,
                FWSSubmitStatus = model.FWSSubmitStatus,
                FWSSubmitTime = model.FWSSubmitTime,
                FWSUserCompleteTime = model.FWSUserCompleteTime,
                FWSUserName = model.FWSUserName,
                FWSUserSubmitStatus = model.FWSUserSubmitStatus,
                FWSUserSubmitTime = model.FWSUserSubmitTime,
                FWSUserTel = model.FWSUserTel,
                Id = model.Id,
                JSXCheckTime = model.JSXCheckTime,
                JXSId = model.JXSId,
                MDId = model.MDId,
                OrderNum = model.OrderNum,
                Province = model.Province,
                Remarks = model.Remarks,
                RequireDeliveryTime = model.RequireDeliveryTime,
                RequireInstallTime = model.RequireInstallTime,
                Status = model.Status,
                StoreId = model.StoreId,
                WLCheckTime = model.WLCheckTime,
                WLDeliverTime = model.WLDeliverTime,
                DGName = model.DG == null ? "" : model.DG.Name,
                FWSName = model.FWS == null ? "" : model.FWS.Name,
                JXSName = model.JXS == null ? "" : model.JXS.Name,
                MDName = model.MD == null ? "" : model.MD.Name,
                StoreName = model.Store == null ? "" : model.Store.Name
            };
        }
        #endregion

        #region 订单管理
        // GET: Order
        public ActionResult OrderList()
        {
            return View();
        }

        public string GetOrderList()
        {
            //string title = Request.QueryString["title"] == null ? "" : Request.QueryString["title"].ToString();
            //int booktype = Request.QueryString["booktype"] == null ? -1 : Convert.ToInt32(Request.QueryString["booktype"]);
            //int knowledgeClass = Request.QueryString["knowledgeClass"] == null ? -1 : Convert.ToInt32(Request.QueryString["knowledgeClass"]);
            //DateTime? begintime = Request.QueryString["begintime"] == null ? new Nullable<DateTime>() : Convert.ToDateTime(Request.QueryString["begintime"]);
            //DateTime? endtime = Request.QueryString["endtime"] == null ? new Nullable<DateTime>() : Convert.ToDateTime(Request.QueryString["endtime"]);
            int pageSize = Convert.ToInt32(Request.QueryString["rows"]);
            int pageIndex = Convert.ToInt32(Request.QueryString["page"]) - 1;
            var data = _orderService.GetOrderList(_workContext.CurrentUser, pageIndex, pageSize);
            List<object> model = data.Select(p => (object)new
            {
                Id = p.Id,
                OrderNum = p.OrderNum,
                StatusId= p.Status,
                FWSUserSubmitStatus=p.FWSUserSubmitStatus,
                Status = EnumExtensions.GetEnumDescription((OrderStatus)p.Status),
                OrderTotal = p.OrderItems.Sum(c => c.Price),
                StoreName = p.Store == null ? "" : p.Store.Name,
                //WLSName = p.w
                JXSName = p.JXS == null ? "" : p.JXS.Name,
                MCName = p.MD == null ? "" : p.MD.Name,
                DGName = p.DG == null ? "" : p.DG.Name,
                FWSName = p.FWS == null ? "" : p.FWS.Name,
                CustomerName = p.CustomerName,
                CustomerTel = p.CustomerTel1 + "/" + p.CustomerTel2,
                Province = p.Province,
                City = p.City,
                County = p.County,
                Address = p.Address,
                CreateTime = p.CreateTime,
                Remarks = p.Remarks,
                CheckTime = p.JSXCheckTime,
                DeliverTime = p.WLDeliverTime
            }).ToList();
            return model.ToJQGridJSON(data.PageIndex, data.PageSize, data.TotalCount);
        }

        public ActionResult AddOrder()
        {
            var model = new OrderModel()
            {
                AvailableDG = _userService.GetUserByType(_workContext.CurrentUser, UserType.DG).ToSelectList(p => p.Name),
                AvailableFWS = _subordinateCompany.GetSubCompanyByType(_workContext.CurrentUser, SubordinateComapnyType.FWS).ToSelectList(p => p.Name),
                AvailableJXS = _subordinateCompany.GetSubCompanyByType(_workContext.CurrentUser, SubordinateComapnyType.JXS).ToSelectList(p => p.Name),
                AvailableMD = _subordinateCompany.GetSubCompanyByType(_workContext.CurrentUser, SubordinateComapnyType.MD).ToSelectList(p => p.Name),
                AvailableStore = _store.GetStore(_workContext.CurrentUser).ToSelectList(p => p.Name)

            };
            return View(model);
        }

        [HttpPost]
        public ActionResult AddOrder(OrderModel order)
        {
            var data = ToOrder(order);
            data.Status = (int)OrderStatus.AreaApprovalPending;
            _orderService.AddOrder(data);
            return Json(new
            {
                success = true,
                message = "添加成功"
            });
        }

        public ActionResult OrderInfo(int OrderId)
        {
            var data = _orderService.GetOrderById(OrderId);
            return View(ToModel(data));
        }
        #endregion

        #region 订单商品
        public string GetOrderItemList(int OrderId)
        {
            int pageSize = Convert.ToInt32(Request.QueryString["rows"]);
            int pageIndex = Convert.ToInt32(Request.QueryString["page"]) - 1;
            var data = _orderService.GetOrderProducts(OrderId, pageIndex, pageSize);
            List<object> model = data.Select(p => (object)new
            {
                Id = p.Id,
                Num = p.Product.Num,
                Name = p.Product.Name,
                Specification = p.Product.Specification,
                CatalogName = p.Product.Catalog.Name,
                Price = p.Price,
                Quantity = p.Quantity,
                Total = p.Quantity * p.Price,
            }).ToList();
            return model.ToJQGridJSON(data.PageIndex, data.PageSize, data.TotalCount);
        }

        public ActionResult AddOrderProduct(int OrderId)
        {
            OrderProductModel model = new OrderProductModel()
            {
                AvailableCatalog = _catalogService.GetAllCataLog(_workContext.CurrentUser).ToSelectList(p => p.Name)
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult AddOrderProduct(OrderProductModel model)
        {
            var product = _productService.getProductById(model.ProductId);
            //var productStore = product.ProductStores.Where(p => p.SubordinateCompanyId == _workContext.CurrentUser.SubordinateCompanyId).FirstOrDefault();
            var data = new OrderItem()
            {
                OrderId = model.OrderId,
                Price = product.SuggestPrice,
                Quantity = model.Quantity,
                ProductId = model.ProductId,
                Remarks = model.Remarks
            };
            _orderService.AddOrderProduct(data);
            return Json(new
            {
                success = true,
                message = "添加成功"
            });
        }

        public ActionResult DelOrderProduct(int Id)
        {
            _orderService.DelOrderProduct(Id);
            return Json(new
            {
                success = true,
                message = "操作成功"
            });
        }

        public ActionResult UpdOrderProduct(int Id)
        {
            var orderItem = _orderService.GetOrderProductById(Id);
            int kccount = orderItem.Product.ProductStores.Where(p => p.SubordinateCompanyId == _workContext.CurrentUser.SubordinateCompanyId).FirstOrDefault().Quantity;
            OrderProductModel model = new OrderProductModel()
            {
                Id = orderItem.Id,
                OrderId = orderItem.OrderId,
                AvailableCatalog = _catalogService.GetAllCataLog(_workContext.CurrentUser).ToSelectList(p => p.Name),
                AvailableProducts = _productService.getProductByCatalog(orderItem.Product.CatalogId).ToSelectList(p => p.Name),
                CatalogId = orderItem.Product.CatalogId,
                ProductId = orderItem.ProductId,
                KYCount = kccount,
                KCCount = kccount,
                Quantity = orderItem.Quantity,
                Remarks = orderItem.Remarks
            };
            return View("AddOrderProduct", model);
        }

        [HttpPost]
        public ActionResult UpdOrderProduct(OrderProductModel model)
        {
            _orderService.UpdOrderProduct(model.Id, model.ProductId, model.Quantity, model.Remarks);
            return Json(new
            {
                success = true,
                message = "操作成功"
            });
        }
        #endregion

        #region 订单审核
        public ActionResult OrderCheckList()
        {
            return View();
        }

        public ActionResult OrderCheckInfo(int OrderId)
        {
            var data = _orderService.GetOrderById(OrderId);
            return View(ToModel(data));
        }

        public ActionResult UpdOrderStatus(int orderId, int status)
        {
            _orderService.UpdOrderStatus(orderId, status);
            return Json(new
            {
                success = true,
                message = "操作成功"
            });
        }

        public ActionResult UpdFWS(int orderId)
        {
            ViewData["AvailableFWS"] = _subordinateCompany.GetSubCompanyByType(_workContext.CurrentUser, SubordinateComapnyType.FWS).ToSelectList(p => p.Name);
            return View();
        }

        [HttpPost]
        public ActionResult UpdFWS(int FWSID, int OrderId)
        {
            _orderService.UpdFWS(OrderId, FWSID);
            return Json(new
            {
                success = true,
                message = "操作成功"
            });
        }

        /// <summary>
        /// 确认回收小票
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public ActionResult ConfirmInvoice(int orderId,int ctype)
        {
            _orderService.ConfirmInvoice(orderId,ctype);
            return Json(new
            {
                success = true,
                message = "操作成功"
            });
        }
        #endregion

        #region 订单查询
        public ActionResult OrderSearchList()
        {
            return View();
        }
        public ActionResult OrderSearchInfo(int OrderId)
        {
            var data = _orderService.GetOrderById(OrderId);
            return View(ToModel(data));
        }
        #endregion

        #region 订单明细查询
        public ActionResult OrderInfoList()
        {
            return View();
        }

        public string GetOrderInfoList()
        {
            int pageSize = Convert.ToInt32(Request.QueryString["rows"]);
            int pageIndex = Convert.ToInt32(Request.QueryString["page"]) - 1;
            var data = _orderService.GetOrderItems(_workContext.CurrentUser, pageIndex, pageSize);
            List<object> model = data.Select(p => (object)new
            {
                Id = p.Id,
                OrderNum = p.Order.OrderNum,
                Status = EnumExtensions.GetEnumDescription((OrderStatus)p.Order.Status),
                //OrderTotal = p.OrderItems.Sum(c => c.Price),
                StoreName = p.Order.Store == null ? "" : p.Order.Store.Name,
                CatalogName = p.Product.Catalog.Name,
                ProductName = p.Product.Name,
                Specification = p.Product.Specification,
                Amount = p.Quantity,
                Price = p.Price,
                Total = p.Quantity * p.Price,
                JXSName = p.Order.JXS == null ? "" : p.Order.JXS.Name,
                MCName = p.Order.MD == null ? "" : p.Order.MD.Name,
                DGName = p.Order.DG == null ? "" : p.Order.DG.Name,
                //WLName=p.Order.
                FWSName = p.Order.FWS == null ? "" : p.Order.FWS.Name,
                CustomerName = p.Order.CustomerName,
                CustomerTel = p.Order.CustomerTel1 + "/" + p.Order.CustomerTel2,
                Province = p.Order.Province,
                City = p.Order.City,
                County = p.Order.County,
                Address = p.Order.Address,
                CreateTime = p.Order.CreateTime,
                CheckTime = p.Order.WLCheckTime,
                DeliverTime = p.Order.WLDeliverTime,
                FWSUserCompleteTime = p.Order.FWSUserCompleteTime,
                ProductNum = p.Product.Num
            }).ToList();
            return model.ToJQGridJSON(data.PageIndex, data.PageSize, data.TotalCount);
        }
        #endregion

        #region 订单配送
        public ActionResult DeliveryList()
        {
            return View();
        }

        public string GetDeliveryList()
        {
            int pageSize = Convert.ToInt32(Request.QueryString["rows"]);
            int pageIndex = Convert.ToInt32(Request.QueryString["page"]) - 1;
            var data = _orderService.GetOrdersByStatus(_workContext.CurrentUser, OrderStatus.FWSApproval, pageIndex, pageSize);
            List<object> model = data.Select(p => (object)new
            {
                Id = p.Id,
                OrderNum = p.OrderNum,
                Status = EnumExtensions.GetEnumDescription((OrderStatus)p.Status),
                OrderTotal = p.OrderItems.Sum(c => c.Price),
                StoreName = p.Store == null ? "" : p.Store.Name,
                //WLSName = p.w
                JXSName = p.JXS == null ? "" : p.JXS.Name,
                MCName = p.MD == null ? "" : p.MD.Name,
                DGName = p.DG == null ? "" : p.DG.Name,
                FWSName = p.FWS == null ? "" : p.FWS.Name,
                CustomerName = p.CustomerName,
                CustomerTel = p.CustomerTel1 + "/" + p.CustomerTel2,
                Province = p.Province,
                City = p.City,
                County = p.County,
                Address = p.Address,
                CreateTime = p.CreateTime,
                Remarks = p.Remarks,
                CheckTime = p.JSXCheckTime,
                DeliverTime = p.WLDeliverTime
            }).ToList();
            return model.ToJQGridJSON(data.PageIndex, data.PageSize, data.TotalCount);
        }
    }
    #endregion
}