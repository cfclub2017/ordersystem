﻿using Domain.Model.Users;
using Domain.Services;
using Domain.Services.Companys;
using Domain.Services.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderSystem.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private IWorkContext _workContext;
        private ISubordinateCompanyService _subordinateCompanyService;
        private IUserService _userService;
        private IRoleService _roleService;
        private IMenuService _menuService;
        private IRoleMenusService _roleMenuService;


        public HomeController(
            IUserService userService,
            ISubordinateCompanyService subordinateCompany,
            IWorkContext workContext,
            IMenuService menuService,
            IRoleService roleService,
            IRoleMenusService roleMenusService
            )
        {
            _userService = userService;
            _subordinateCompanyService = subordinateCompany;
            _workContext = workContext;
            _roleService = roleService;
            _menuService = menuService;
            _roleMenuService = roleMenusService;
        }
        public ActionResult Index()
        {
            List<Menu> menuList = _menuService.GetAllMenu();
            ViewData["RoleMenus"] = _userService.GetUserByUsername(_workContext.CurrentUser.UserName).Role.RoleMenusList;
            ViewData["AllMenu"] = menuList;
            return View();
        }

        public ActionResult HomePage()
        {
            return View();
        }
    }
}
