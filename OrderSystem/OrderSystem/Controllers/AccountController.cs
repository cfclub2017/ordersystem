﻿using Common;
using Common.Enums;
using Domain.Model.Comapnys;
using Domain.Model.Common;
using Domain.Model.Users;
using Domain.Services;
using Domain.Services.Companys;
using Domain.Services.Products;
using Domain.Services.Users;
using Microsoft.Owin.Security;
using OrderSystem.Extensions;
using OrderSystem.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace OrderSystem.Controllers
{
    public class AccountController : Controller
    {
        private IWorkContext _workContext;
        private ISubordinateCompanyService _subordinateCompanyService;
        private IUserService _userService;
        private IRoleService _roleService;
        private IMenuService _menuService;
        private IRoleMenusService _roleMenuService;


        public AccountController(
            IUserService userService,
            ISubordinateCompanyService subordinateCompany,
            IWorkContext workContext,
            IMenuService menuService,
            IRoleService roleService,
            IRoleMenusService roleMenusService
            )
        {
            _userService = userService;
            _subordinateCompanyService = subordinateCompany;
            _workContext = workContext;
            _roleService = roleService;
            _menuService = menuService;
            _roleMenuService = roleMenusService;
        }
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(Models.LoginModel login)
        {
            if (String.IsNullOrWhiteSpace(login.UserName))
            {
                ModelState.AddModelError("", "请输入用户名。");
                return View(login);
            }
            if (String.IsNullOrWhiteSpace(login.Password))
            {
                ModelState.AddModelError("", "密码不能为空。");
                return View(login);
            }
            //if (String.Compare(GetCode(), login.ValidateCode, true) != 0)
            //{
            //    ModelState.AddModelError("", "验证码错误，请输入正确的验证码。");
            //    return View(login);
            //}


           UserAccount user =_userService.GetUserByUsername(login.UserName);

            if (user == null || user.UserPwd != login.Password.Trim())
            {
                ModelState.AddModelError("", "用户名或密码错误。");
                return View(login);
            }
            //if (user.UserType != (int)Domain.Model.EUserType.SYSTEMADMIN && user.UserType != (int)Domain.Model.EUserType.COMPANYADMIN)
            //{
            //    ModelState.AddModelError("", "您无权登录当前系统。");
            //    return View(login);
            //}


            ClaimsIdentity _identity = new ClaimsIdentity(CookicNames.AdminCookie);
            //_identity.AddClaim(new Claim("UserType", user.UserType.ToString()));
            _identity.AddClaim(new Claim("UserType", user.UserType.ToString()));
            _identity.AddClaim(new Claim("CompanyId", user.CompanyID.ToString()));
            _identity.AddClaim(new Claim(ClaimTypes.Name, user.Name.ToString()));
            _identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
            //_identity.AddClaim(new Claim("DisplayName", user.DisplayName));
            HttpContext.GetOwinContext().Authentication.SignIn(new AuthenticationProperties() { IsPersistent = login.RememberMe }, _identity);
            _workContext.CurrentUser = user;
            ////定义身份验证 Cookie
            //IAuthenticationService authenticationService = ObjectContainer.Resolve<IAuthenticationService>();
            //authenticationService.SignIn(user, login.RememberMe);
            return RedirectToAction("Index", "Home");
        }
        //[AllowAnonymous]
        //public ActionResult GetImageCode()
        //{
        //    ValidateCode vCode = new ValidateCode();
        //    string code = vCode.CreateValidateCode(4);
        //    Session["ValidateCode"] = code;
        //    byte[] bytes = vCode.CreateValidateGraphic(code);
        //    return File(bytes, @"image/jpeg");
        //}
        //public string GetCode()
        //{
        //    return Session["ValidateCode"].ToString();
        //}

        public ActionResult LogOut()
        {
            HttpContext.GetOwinContext().Authentication.SignOut(CookicNames.AdminCookie);
            _workContext.CurrentUser = null;
            return RedirectToAction("Login", "Account");
        }

        public ActionResult UpdMyPwd()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UpdMyPwd(string txtOldPwd, string txtNewPwd, string txtCNewPwd)
        {
            //if (txtCNewPwd != txtNewPwd)
            //    throw new Exception("两次密码输入不匹配！");


            //string tmp = Common.Security.Encryption.EncryptMd5Text(txtOldPwd.Trim());
            //if (user.UserPwd != tmp)
            //    throw new Exception("原始密码错误！");

            //var userService = new UserService();
            //userService.UpdMyUserPwd(user, txtNewPwd);
            return Content("ok");
        }

        #region 用户管理
        public ActionResult UserAccountList()
        {
            return View();
        }
        public ActionResult ValidateUserName() {
            string UserName = Request["UserName"];
            if(_userService.GetUserByUsername(UserName) != null)
                return Json(false);
            else
                return Json(true);
        }

        public string GetUserAccountList()
        {
            int pageSize = Convert.ToInt32(Request.QueryString["rows"]);
            int pageIndex = Convert.ToInt32(Request.QueryString["page"]) - 1;
            UserAccount parms = new UserAccount();
            parms.UserName = Request.QueryString["UserName"];
            parms.RoleID = Convert.ToInt32(Request.QueryString["RoleID"]);
            parms.UserState = Convert.ToInt32(Request.QueryString["UserState"]);
            var data = _userService.GetUserAccountList(parms, pageIndex, pageSize);
            List<object> model = data.Select(p => (object)new
            {
                Id = p.Id,
                Name = p.Name,
                UserState = EnumExtensions.GetEnumDescription((UserStatus)p.UserState),
                Tel = p.Tel,
                UserName = p.UserName,
                UserPwd = p.UserPwd,
                SubordinateCompanyId = p.SubordinateCompanyId,
                CompanyID = p.CompanyID,
                UserType = EnumExtensions.GetEnumDescription((UserType)p.UserType),
                CreateTime = p.CreateTime,
                RoleID = p.RoleID

            }).ToList();
            return model.ToJQGridJSON(data.PageIndex, data.PageSize, data.TotalCount);
        }

        public ActionResult AddAccount()
        {
            var model = new UserAccountModel();

            model.UserTypeList = EnumExtensions.GetSelectList<UserType>();
            model.UserStateList = EnumExtensions.GetSelectList<UserStatus>();
            model.UserRoleList = _roleService.GetAll().ToSelectList(p => p.Name);
            model.CompanyList =
                //SelectListHelper.ToSelectList<SubordinateCompany>(_subordinateCompany.GetSubCompanyByType(_workContext.CurrentUser, SubordinateComapnyType.FWS).GetEnumerator());
                _subordinateCompanyService.GetSubCompanyByType(_workContext.CurrentUser, SubordinateComapnyType.ALL).ToSelectList(p => p.Name);

            return View(model);
        }

        [HttpPost]
        public ActionResult AddAccount(UserAccount user)
        {
            _userService.AddUserAccount(user);
            return Json(new
            {
                success = true,
                message = "添加成功"
            });
        }

        [HttpPost]
        public ActionResult DelAccount(int id)
        {
            _userService.DelUserAccount(id);
            return Json(new
            {
                success = true,
                message = "删除成功"
            });
        }

        public ActionResult UpdAccount(int id)
        {
            var user = _userService.GetUserById(id);
            UserAccountModel model =  new UserAccountModel();
            model.Id = user.Id;
            model.Name = user.Name;
            model.UserName = user.UserName;
            model.UserState = user.UserState;
            model.UserType = user.UserType;
            model.Tel = user.Tel;
            model.CompanyID = user.CompanyID;
            model.UserPwd = user.UserPwd;
           
            List<SelectListItem> items = EnumExtensions.GetSelectList<UserType>();
            foreach (SelectListItem item in items)
            {
                if (item.Value == Convert.ToString(model.UserType))
                {
                    item.Selected = true;
                }
            }
            model.UserTypeList = items;


            items = EnumExtensions.GetSelectList<UserStatus>();
            foreach (SelectListItem item in model.UserStateList)
            {
                if (item.Value == Convert.ToString(model.UserState))
                {
                    item.Selected = true;
                }
            }
            model.UserStateList = items;
            model.CompanyList =
                _subordinateCompanyService.GetSubCompanyByType(_workContext.CurrentUser, SubordinateComapnyType.FWS).ToSelectList(p => p.Name);
            return View(model);
        }

        [HttpPost]
        public ActionResult UpdAccount(UserAccount user)
        {
            _userService.UpdUserAccount(user);
            return Json(new
            {
                success = true,
                message = "修改成功"
            });
        }

        [HttpPost]
        public ActionResult StopAccount(int id)
        {
            _userService.StopUserAccount(id);
            return Json(new
            {
                success = true,
                message = "停用成功"
            });
        }
        #endregion

        #region 角色管理
        public ActionResult RoleList()
        {
            return View();
        }

        public string GetRoleList()
        {
            int pageSize = Convert.ToInt32(Request.QueryString["rows"]);
            int pageIndex = Convert.ToInt32(Request.QueryString["page"]) - 1;
            Role parms = new Role();
            parms.Name = Request.QueryString["Name"];
            var data = _roleService.GetRoleList(parms, pageIndex, pageSize);

            List<object> model = data.Select(p => (object)new
            {
                Id = p.Id,
                Name = p.Name,
                Remarks = p.Remarks,
                Code = p.Code                
            }).ToList();
            return model.ToJQGridJSON(data.PageIndex, data.PageSize, data.TotalCount);
        }

        public ActionResult AddRole()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddRole(Role model)
        {
                _roleService.Add(model);
                return Json(new
                {
                    success = true,
                    message = "添加成功"
                });
        }

        [HttpPost]
        public ActionResult DelRole(int id)
        {
            _roleService.Del(id);
            return Json(new
            {
                success = true,
                message = "删除成功"
            });
        }

        public ActionResult UpdRole(int id)
        {
            var Role = _roleService.GetById(id);
            Role model = new Role();
            model.Id = Role.Id;
            model.Name = Role.Name;
            model.Remarks = Role.Remarks;
            model.Code = Role.Code;
            return View(model);
        }

        [HttpPost]
        public ActionResult UpdRole(Role user)
        {
            _roleService.Upd(user);
            return Json(new
            {
                success = true,
                message = "修改成功"
            });
        }

        //角色菜单设置
        public ActionResult SetRoleMenus(int id)
        {
            List<Menu> menuList = _menuService.GetAllMenu().Where<Menu>(p => { return p.Status == (int)Status.Pending; }).ToList();
            ViewData["menuList"] = menuList;
            List<int> checkMenuList = _roleMenuService.GetByID(id).Select(p=>p.MenuID).ToList();
            ViewData["checkMenuList"] = checkMenuList;
            ViewData["id"] = id;
            return View();
        }

        [HttpPost]
        public ActionResult SetRoleMenus(Role user)
        {
            string[] ids = Convert.ToString(Request["menuIds"]).Split(',');
            List<RoleMenus> r = new List<RoleMenus>();
            RoleMenus rm = new RoleMenus();
            foreach (string id in ids) {
                if (!string.IsNullOrEmpty(id)) {
                    rm = new RoleMenus();
                    rm.MenuID = Convert.ToInt32(id);
                    rm.RoleID = user.Id;
                    rm.CreateTime = DateTime.Now;
                    r.Add(rm);
                }
           }
            _roleMenuService.Upd(r, user.Id);
            //_roleMenuService.UpdOne(r[0], user.Id);
            return Json(new
            {
                success = true,
                message = "分配成功"
            });
        }
        #endregion

        #region 菜单管理
        public ActionResult MenuList()
        {
            List<Menu> source = _menuService.GetAllMenu();
            List<Menu> desc = new List<Menu>();
            sortedList(desc, source,0);
            ViewData["menuList"] = desc;
            return View();
        }

        private void sortedList(List<Menu> desc, List<Menu>source, int parentId) {
            for (int i = 0; i < source.Count; i++)
            {
                Menu e = source[i];
                if (e != null && e.ParentId == parentId)
                {
                    desc.Add(e);
                    if (true)
                    {
                        // 判断是否还有子节点, 有则继续获取子节点
                        for (int j = 0; j < source.Count; j++)
                        {
                            Menu child = source[j];
                            if (child.Id != 0
                                    && child.ParentId == e.Id)
                            {
                                sortedList(desc, source, e.Id);
                                break;
                            }
                        }
                    }
                }
            }
        }

        public string GetMenuList()
        {
            int pageSize = Convert.ToInt32(Request.QueryString["rows"]);
            int pageIndex = Convert.ToInt32(Request.QueryString["page"]) - 1;
            Menu parms = new Menu();
            parms.Name = Request.QueryString["Name"];
            var data = _menuService.GetMenuList(parms, pageIndex, pageSize);

            List<object> model = data.Select(p => (object)new
            {
                Id = p.Id,
                Name = p.Name,
                OrderNo = p.OrderNo,
                ActionName = p.ActionName,
                ControllerName = p.ControllerName,
                IconClass = p.IconClass,
                ParentId = p.ParentId,
                Remarks = p.Remarks,
                IfShow = EnumExtensions.GetEnumDescription((IfShow)p.IfShow),
                Status = EnumExtensions.GetEnumDescription((Status)p.Status),
                CreateTime = p.CreateTime
            }).ToList();
            return model.ToJQGridJSON(data.PageIndex, data.PageSize, data.TotalCount);
        }

        public ActionResult AddMenu()
        {
            MenuModel model = new MenuModel();
            if(string.IsNullOrEmpty(Request.QueryString["parentId"]))
                model.ParentId = 0;
            else
                model.ParentId = Convert.ToInt32(Request.QueryString["parentId"] );
            model.StatusList = EnumExtensions.GetSelectList<Status>();
            model.IfSowList = EnumExtensions.GetSelectList<IfShow>();
            return View(model);
        }

        [HttpPost]
        public ActionResult AddMenu(Menu model)
        {
            _menuService.Add(model);
            List<SelectListItem> items = EnumExtensions.GetSelectList<Status>();
            foreach (SelectListItem item in items)
            {
                if (item.Value == Convert.ToString(model.Status))
                {
                    item.Selected = true;
                }
            }
            MenuModel menu = new MenuModel();
            menu.StatusList = items;
            items = EnumExtensions.GetSelectList<IfShow>();
            foreach (SelectListItem item in items)
            {
                if (item.Value == Convert.ToString(model.IfShow))
                {
                    item.Selected = true;
                }
            }
            menu.IfSowList = items;
            return Json(new
            {
                success = true,
                message = "添加成功"
            });
        }

        [HttpPost]
        public ActionResult DelMenu(int id)
        {
            _menuService.Del(id);
            return Json(new
            {
                success = true,
                message = "删除成功"
            });
        }

        public ActionResult UpdMenu(int id)
        {
            var Menu = _menuService.GetById(id);
            MenuModel model = new MenuModel();
            model.Id = Menu.Id;
            model.Name = Menu.Name;
            model.Remarks = Menu.Remarks;
            model.OrderNo = Menu.OrderNo;
            model.ActionName = Menu.ActionName;
            model.ControllerName = Menu.ControllerName;
            model.IconClass = Menu.IconClass;
            model.ParentId = Menu.ParentId;
            model.Status = Menu.Status;
            model.CreateTime = Menu.CreateTime;
            model.IfShow = Menu.IfShow;

            List<SelectListItem> items = EnumExtensions.GetSelectList<Status>();
            foreach (SelectListItem item in items)
            {
                if (item.Value == Convert.ToString(model.Status))
                {
                    item.Selected = true;
                }
            }

            model.StatusList = items;
            items = EnumExtensions.GetSelectList<IfShow>();
            foreach (SelectListItem item in items)
            {
                if (item.Value == Convert.ToString(model.IfShow))
                {
                    item.Selected = true;
                }
            }

            model.IfSowList = items;
            return View(model);
        }

        [HttpPost]
        public ActionResult UpdMenu(Menu user)
        {
            _menuService.Upd(user);
            return Json(new
            {
                success = true,
                message = "修改成功"
            });
        }
        #endregion
    }
}