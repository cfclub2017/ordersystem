﻿using Common;
using Common.Enums;
using Domain.Model.Comapnys;
using Domain.Model.Common;
using Domain.Model.Users;
using Domain.Services;
using Domain.Services.Comapnys;
using Domain.Services.Companys;
using Domain.Services.Users;
using Microsoft.Owin.Security;
using OrderSystem.Extensions;
using OrderSystem.Models.SubordinateCompany;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace OrderSystem.Controllers
{
    public class SubordinateCompanyController : Controller
    {
        private IWorkContext _workContext;
        private ISubordinateCompanyService _subordinateCompanyService;
        private ICompanyService _iCompanyService;

        public SubordinateCompanyController(
            ISubordinateCompanyService subordinateCompany,
            IWorkContext workContext,ICompanyService iCompanyService)
        {
            _subordinateCompanyService = subordinateCompany;
            _workContext = workContext;
            _iCompanyService = iCompanyService;
        }
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult SubordinateCompanyList()
        {
            return View();
        }

        #region 经销商管理
        public ActionResult JXSCompanyList()
        {
            return View();
        }
        public ActionResult MCCompanyList()
        {
            return View();
        }
        public ActionResult WLSCompanyList()
        {
            return View();
        }
        public ActionResult FUSCompanyList()
        {
            return View();
        }

        public string GetSubordinateCompanyList()
        {
            int pageSize = Convert.ToInt32(Request.QueryString["rows"]);
            int pageIndex = Convert.ToInt32(Request.QueryString["page"]) - 1;
            SubordinateCompany parms = new SubordinateCompany();
            parms.Name = Request.QueryString["Name"];
            parms.Status = Convert.ToInt32(Request.QueryString["Status"]);
            parms.CompanyType = Convert.ToInt32(Request.QueryString["CompanyType"]);
            var data = _subordinateCompanyService.GetSubordinateCompanyList(parms, pageIndex, pageSize);

            List<object> model = data.Select(p => (object)new
            {
                Id = p.Id,
                Name = p.Name,
                Num = p.Num,
                Status = EnumExtensions.GetEnumDescription((Status)p.Status),
                Contacts = p.Contacts,
                ContactTel1 = p.ContactTel1,
                ContactTel2 = p.ContactTel2,
                Province = p.Province,
                City = p.City,
                County = p.County,
                Address = p.Address,
                ParentId = p.ParentId,
                CompanyType = EnumExtensions.GetEnumDescription((SubordinateComapnyType)p.CompanyType),
                CreateTime = p.CreateTime,
                CompanyId = p.CompanyId,
                Remarks = p.Remarks
        }).ToList();
            return model.ToJQGridJSON(data.PageIndex, data.PageSize, data.TotalCount);
        }

        public ActionResult AddSubordinateCompany()
        {
            var model = new SubordinateCompanyModel();

            model.SubordinateCompanyTypeList = EnumExtensions.GetSelectList<Domain.Model.Comapnys.SubordinateComapnyType>();
            model.StatusList = EnumExtensions.GetSelectList<Status>();
            Comapny parm = new Comapny();
            model.CompanyList =
                _iCompanyService.GetComapnyList(parm, 0,100).ToSelectList(p => p.Name);
            model.ParCompanyList =
                _subordinateCompanyService.GetSubCompanyByType(_workContext.CurrentUser, SubordinateComapnyType.ALL).ToSelectList(p => p.Name);
            model.CompanyType = Convert.ToInt32(Request.QueryString["CompanyType"]);
            return View(model);
        }

        [HttpPost]
        public ActionResult AddSubordinateCompany(SubordinateCompany user)
        {
            user.Comapny = _iCompanyService.GetById(user.CompanyId);
            _subordinateCompanyService.AddSubordinateCompany(user);
            return Json(new
            {
                success = true,
                message = "添加成功"
            });
        }

        [HttpPost]
        public ActionResult DelSubordinateCompany(int id)
        {
            _subordinateCompanyService.DelSubordinateCompany(id);
            return Json(new
            {
                success = true,
                message = "删除成功"
            });
        }

        public ActionResult UpdSubordinateCompany(int id)
        {
            var user = _subordinateCompanyService.GetSubCompanyById(id);
            SubordinateCompanyModel model = new SubordinateCompanyModel();
            model.Id = user.Id;
            model.Name = user.Name;
                model.Num = user.Num;

                model.Contacts = user.Contacts;

                model.ContactTel1 = user.ContactTel1;

                model.ContactTel2 = user.ContactTel2;

                model.Province = user.Province;

                model.City = user.City;

                model.County = user.County;

                model.Address = user.Address;

                model.Remarks = user.Remarks;

                model.Status = user.Status;

                model.ParentId = user.ParentId;

                model.CompanyType = user.CompanyType;

                model.CompanyId = user.CompanyId;

            List<SelectListItem> items = EnumExtensions.GetSelectList<Status>();
            foreach (SelectListItem item in items)
            {
                if (item.Value == Convert.ToString(model.Status))
                {
                    item.Selected = true;
                }
            }
            model.StatusList = items;


            items = EnumExtensions.GetSelectList<SubordinateComapnyType>();
            foreach (SelectListItem item in model.SubordinateCompanyTypeList)
            {
                if (item.Value == Convert.ToString(model.CompanyType))
                {
                    item.Selected = true;
                }
            }
            model.SubordinateCompanyTypeList = items;
                 
            items = _iCompanyService.GetComapnyList(new Comapny(), 0, 100).ToSelectList(p => p.Name);
            foreach (SelectListItem item in model.CompanyList)
            {
                if (item.Value == Convert.ToString(model.CompanyId))
                {
                    item.Selected = true;
                }
            }
            model.CompanyList = items;

            items = _subordinateCompanyService.GetSubCompanyByType(_workContext.CurrentUser, SubordinateComapnyType.ALL).ToSelectList(p => p.Name);
            foreach (SelectListItem item in model.SubordinateCompanyTypeList)
            {
                if (item.Value == Convert.ToString(model.ParentId))
                {
                    item.Selected = true;
                }
            }
            model.ParCompanyList = items;
            return View(model);
        }

        [HttpPost]
        public ActionResult UpdSubordinateCompany(SubordinateCompany user)
        {
            _subordinateCompanyService.UpdSubordinateCompany(user);
            return Json(new
            {
                success = true,
                message = "修改成功"
            });
        }

        [HttpPost]
        public ActionResult StopSubordinateCompany(int id)
        {
            _subordinateCompanyService.StopSubordinateCompany(id);
            return Json(new
            {
                success = true,
                message = "停用成功"
            });
        }
        #endregion
    }
}