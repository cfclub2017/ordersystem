﻿using Common.Enums;
using Domain.Model.Orders;
using Domain.Model.Stores;
using Domain.Services;
using Domain.Services.Companys;
using Domain.Services.Stores;
using Domain.Services.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderSystem.Controllers
{
    public class StoreController : Controller
    {
        private IWorkContext _workContext;
        private ISubordinateCompanyService _subordinateCompany;
        private IUserService _userService;
        private IStoreService _store;
        private IInStoreService _inStoreService;
        public StoreController(
            IInStoreService inStoreService,
            IStoreService store,
            IUserService userService,
            ISubordinateCompanyService subordinateCompany,
            IWorkContext workContext)
        {
            _inStoreService = inStoreService;
            _store = store;
            _userService = userService;
            _subordinateCompany = subordinateCompany;
            _workContext = workContext;
        }

        #region 入库单
        // GET: Store
        public ActionResult InStoreOrderList()
        {
            return View();
        }

        public string GetInStoreOrderList()
        {
            //int pageSize = Convert.ToInt32(Request.QueryString["rows"]);
            //int pageIndex = Convert.ToInt32(Request.QueryString["page"]) - 1;
            //InStore parms = new InStore();
            //parms.Num = Convert.ToInt32(Request.QueryString["Num"]);
            //parms.ParentId = Convert.ToInt32(Request.QueryString["ParentId"]);
            //parms.Level = Convert.ToInt32(Request.QueryString["Level"]);
            //var data = _inStoreService.GetInStoreOrderList(parms, pageIndex, pageSize);

            //List<object> model = data.Select(p => (object)new
            //{
            //    Id = p.Id,
            //    Name = p.Name,
            //    Num = p.Num,
            //    ParentId = p.ParentId,
            //    ParentName = p.ParentName,
            //    CompanyName = p.CompanyName,
            //    Remarks = p.Remarks,
            //    Level = EnumExtensions.GetEnumDescription((Level)p.Level),
            //    Icon = p.Icon,
            //    ImageMimeType = p.ImageMimeType
            //}).ToList();
            //return model.ToJQGridJSON(data.PageIndex, data.PageSize, data.TotalCount);
            return null;
        }
        #endregion
    }
}