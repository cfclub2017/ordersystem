﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Web;

namespace Common
{
    public static class HttpHelper
    {
        public class error
        {
            public string ExceptionType { get; set; }
            public string ExceptionMessage { get; set; }
        }
    public static string PostData(string sendUrl, string strPost,string contentType= "application/json", string ProxyStr=null)
        {
            try
            {
                byte[] buffer = Encoding.UTF8.GetBytes(strPost);
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(sendUrl);
                if (ProxyStr != "" && ProxyStr != null)
                {
                    //设置代理
                    WebProxy proxy = new WebProxy();
                    proxy.Address = new Uri(ProxyStr);
                    request.UseDefaultCredentials = true;
                    request.Proxy = proxy;
                    request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; Trident/4.0; GTB7.4; GTB7.1; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; InfoPath.2)";
                }

                request.Method = "POST";
                request.ContentType = contentType;
                request.ContentLength = buffer.Length;
                Stream requestStram = request.GetRequestStream();
                requestStram.Write(buffer, 0, buffer.Length);
                requestStram.Close();
                Stream getStream = request.GetResponse().GetResponseStream();
                byte[] resultByte = new byte[1024];
                getStream.Read(resultByte, 0, resultByte.Length);

                var content = Encoding.UTF8.GetString(resultByte);
                getStream.Close();
                request.Abort();
                return content;
            }
            catch (WebException e)
            {

                throw new Exception("无法连接服务器" + e.Message);

            }

            //var urlEncodedContent = new System.Net.Http.StringContent(strPost);
            //var httpClient = new System.Net.Http.HttpClient();
            //var result = httpClient.PostAsync(sendUrl, urlEncodedContent).Result.Content.ReadAsStringAsync().Result;
            //var error = _json.Deserialize<SPError>(result);
            //return error;
        }
    }
}
