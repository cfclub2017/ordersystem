﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Newtonsoft.Json;

namespace Common
{
    public static class PagedResultExtension
    {
        public static string ToJQGridJSON<T>(this IPagedList<T> PagedResult)
        {
           return JsonConvert.SerializeObject(new JQGridPagedResult<T>(PagedResult.PageIndex, PagedResult, PagedResult.TotalPages, PagedResult.TotalCount));
        }

        public static string ToJQGridJSON(this IList<object> PagedResult,int pageIndex,int totalPages,int totalCount)
        {
            return JsonConvert.SerializeObject(new JQGridPagedResult<object>(pageIndex, PagedResult, totalPages, totalCount));
        }
    }

    public class JQGridPagedResult<T>
    {
        public JQGridPagedResult(int _page,IList<T> _rows,int _total ,int _records)
        {
            page = _page;
            rows = _rows;
            total = _total;
            records = _records;
        }
        
        public int page { get; set; }
        public IList<T> rows { get; set; }
        public int total { get; set; }
        public int records { get; set; }
    }

    public class JQGridPagedResultDT
    {
        public JQGridPagedResultDT(int _page, System.Data.DataTable _rows, int _total, int _records)
        {
            page = _page;
            rows = _rows;
            total = _total;
            records = _records;
        }

        public int page { get; set; }
        public System.Data.DataTable rows { get; set; }
        public int total { get; set; }
        public int records { get; set; }
    }
}
