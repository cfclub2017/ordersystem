﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    /// <summary>  
    /// 自定义注解属性  
    /// </summary>  
    public class SelectDisplayNameAttribute : Attribute
    {
        private string _diaplayName;
        public string DisplayName
        {
            get
            {
                return _diaplayName;
            }
        }
        public SelectDisplayNameAttribute(string displayName)
        {
            _diaplayName = displayName;
        }

    }
       
}
