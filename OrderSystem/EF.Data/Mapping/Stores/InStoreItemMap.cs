﻿using Domain.Model.Products;
using EF.Data.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Stores
{
    public class InStoreItemMap : EFEntityTypeConfiguration<InStoreItem>
    {
        public InStoreItemMap()
        {
            this.HasKey(t => t.Id);
            this.HasRequired(t => t.InStore)
           .WithMany()
           .HasForeignKey(t => t.InStoreId);
            this.HasRequired(t => t.Product)
           .WithMany()
           .HasForeignKey(t => t.ProductId)
            .WillCascadeOnDelete(false);
        }
    }
}
