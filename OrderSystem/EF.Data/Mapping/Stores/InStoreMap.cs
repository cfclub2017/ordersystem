﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Domain.Model.Comapnys;
using Domain.Model.Users;
using EF.Data.Mapping;

namespace Domain.Model.Stores
{
    public class InStoreMap : EFEntityTypeConfiguration<InStore>
    {
        public InStoreMap()
        {
            this.HasKey(t => t.Id);
            this.HasRequired(t => t.CreateUser)
         .WithMany()
         .HasForeignKey(t => t.CreateUserId)
          .WillCascadeOnDelete(false);
            this.HasRequired(t => t.SubordinateCompany)
         .WithMany()
         .HasForeignKey(t => t.SubordinateCompanyId);
        }
    }
}
