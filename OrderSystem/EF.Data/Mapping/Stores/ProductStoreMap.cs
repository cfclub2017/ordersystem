﻿using Domain.Model.Comapnys;
using EF.Data.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Stores
{
    public class ProductStoreMap : EFEntityTypeConfiguration<ProductStore>
    {
        public ProductStoreMap()
        {
            this.HasKey(t => t.Id);
            this.HasRequired(t => t.SubordinateCompany)
             .WithMany()
            .HasForeignKey(t => t.SubordinateCompanyId);
            this.HasRequired(t => t.Product)
            .WithMany(p=>p.ProductStores)
            .HasForeignKey(t => t.ProductId);
            this.HasRequired(t => t.Store)
            .WithMany()
            .HasForeignKey(t => t.StoreId);
        }
    }
}
