﻿using Domain.Model.Comapnys;
using EF.Data.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Stores
{
    public class StoreMap : EFEntityTypeConfiguration<Store>
    {
        public StoreMap()
        {
            this.HasKey(t => t.Id);
            this.HasRequired(t => t.Facilitator)
            .WithMany()
            .HasForeignKey(t => t.FacilitatorId)
            .WillCascadeOnDelete(false);
        }
    }
}
