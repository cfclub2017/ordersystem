using EF.Data.Mapping;

namespace Domain.Model.Media
{
    /// <summary>
    /// Represents a picture
    /// </summary>
    public partial class PictureMap : EFEntityTypeConfiguration<Picture>
    {
        public PictureMap()
        {
            this.HasKey(t => t.Id);
        }
    }
}
