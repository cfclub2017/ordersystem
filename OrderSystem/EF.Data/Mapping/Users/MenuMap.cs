﻿using EF.Data.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Users
{
    public class MenuMap : EFEntityTypeConfiguration<Menu>
    {
        public MenuMap()
        {
            this.HasKey(t => t.Id);
        }
    }
}
