﻿using EF.Data.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Users
{
    public class RoleMap : EFEntityTypeConfiguration<Role>
    {
        public RoleMap()
        {
            this.HasKey(t => t.Id);
            this.HasMany(t => t.RoleMenusList).WithRequired(p => p.Role);
        }
    }
}
