using Domain.Model.Tasks;
using Domain.Model.Users;

namespace EF.Data.Mapping.Tasks
{
    public partial class UserAccountMap : EFEntityTypeConfiguration<UserAccount>
    {
        public UserAccountMap()
        {
            this.ToTable("UserAccount");
            this.HasKey(t => t.Id);
            this.Property(t => t.UserName).IsRequired();
            this.HasRequired(t => t.SubordinateCompany)
            .WithMany();
            this.HasRequired(t => t.Role)
            .WithMany();
        }
    }
}