﻿using EF.Data.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Users
{
    public class RoleMenusMap : EFEntityTypeConfiguration<RoleMenus>
    {
        public RoleMenusMap()
        {
            this.ToTable("RoleMenus");
            this.HasKey(t => t.Id);
            this.HasRequired(t => t.Role)
            .WithMany()
            .HasForeignKey(t => t.RoleID);
            this.HasRequired(t => t.Menu)
            .WithMany()
            .HasForeignKey(t => t.MenuID);
        }
    }
}
