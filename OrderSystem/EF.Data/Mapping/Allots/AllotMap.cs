﻿using Domain.Model.Comapnys;
using Domain.Model.Products;
using Domain.Model.Users;
using EF.Data.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Allots
{
    public class AllotMap : EFEntityTypeConfiguration<Allot>
    {
        public AllotMap()
        {
            this.HasKey(t => t.Id);
            this.HasRequired(t => t.CreateUser)
                 .WithMany()
                 .HasForeignKey(t => t.CreateUserId)
                 .WillCascadeOnDelete(false);
            this.HasRequired(t => t.FromStore)
                .WithMany()
                .HasForeignKey(t => t.FromStoreId)
             .WillCascadeOnDelete(false);
            this.HasRequired(t => t.ToStore)
               .WithMany()
               .HasForeignKey(t => t.ToStoreId)
             .WillCascadeOnDelete(false);
            this.HasRequired(t => t.SubordinateCompany)
               .WithMany()
               .HasForeignKey(t => t.SubordinateCompanyId);
        }
    }
}
