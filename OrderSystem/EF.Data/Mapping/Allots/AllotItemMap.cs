﻿using Domain.Model.Products;
using EF.Data.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Allots
{
    public class AllotItemMap : EFEntityTypeConfiguration<AllotItem>
    {
        public AllotItemMap()
        {
            this.HasKey(t => t.Id);
            this.HasRequired(o => o.Allot)
               .WithMany()
               .HasForeignKey(o => o.AllotId);
        }
    }
}
