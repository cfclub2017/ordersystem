﻿using EF.Data.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Orders
{
    public class OrderItemMap : EFEntityTypeConfiguration<OrderItem>
    {
        public OrderItemMap()
        {
            this.HasKey(t => t.Id);
            this.HasRequired(t => t.Order)
                        .WithMany()
                        .HasForeignKey(t => t.OrderId);
            this.HasRequired(t => t.Product)
                       .WithMany()
                       .HasForeignKey(t => t.ProductId)
                       .WillCascadeOnDelete(false);
            this.HasRequired(t => t.Order)
                       .WithMany(p => p.OrderItems)
                       .HasForeignKey(t => t.OrderId);
        }
    }
}
