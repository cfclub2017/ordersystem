﻿using Domain.Model.Comapnys;
using Domain.Model.Media;
using Domain.Model.Users;
using EF.Data.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Orders
{
    public class OrderMap : EFEntityTypeConfiguration<Order>
    {
        public OrderMap()
        {
            this.HasKey(t => t.Id);
            this.HasRequired(t => t.CreateUser)
               .WithMany()
               .HasForeignKey(t => t.CreateUserId);
            this.HasOptional(t => t.DG)
              .WithMany()
              .HasForeignKey(t => t.DGId)
               .WillCascadeOnDelete(false);
            this.HasOptional(t => t.JXS)
              .WithMany()
              .HasForeignKey(t => t.JXSId)
               .WillCascadeOnDelete(false); ;
            this.HasOptional(t => t.MD)
              .WithMany()
            .HasForeignKey(t => t.MDId)
             .WillCascadeOnDelete(false);
            this.HasOptional(t => t.Agent)
              .WithMany()
            .HasForeignKey(t => t.AgentId)
             .WillCascadeOnDelete(false);
            this.HasOptional(t => t.Area)
              .WithMany()
            .HasForeignKey(t => t.AreaId)
             .WillCascadeOnDelete(false);
            this.HasOptional(t => t.FWS)
              .WithMany()
            .HasForeignKey(t => t.FWSID)
             .WillCascadeOnDelete(false);
            this.HasOptional(t => t.Store)
              .WithMany()
              .HasForeignKey(t => t.StoreId);
            this.HasMany(t=>t.OrderPictures)
                .WithMany()
                .Map(m => m.ToTable("Order_Pictures_Mapping"));
        }
    }
}
