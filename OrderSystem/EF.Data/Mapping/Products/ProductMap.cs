﻿using Domain.Model.Users;
using EF.Data.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Products
{
    public class ProductMap : EFEntityTypeConfiguration<Product>
    {
        public ProductMap()
        {
            this.HasKey(t => t.Id);
            this.HasRequired(t => t.CreateUser)
           .WithMany()
           .HasForeignKey(t => t.UserId)
           .WillCascadeOnDelete(false);
            this.HasRequired(t => t.Catalog)
          .WithMany();
        }
    }
}
