﻿using EF.Data.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Comapnys
{
    public class ComapnyMap : EFEntityTypeConfiguration<Comapny>
    {
        public ComapnyMap()
        {
            this.HasKey(t => t.Id);
        }
    }
}
