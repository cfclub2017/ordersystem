﻿using EF.Data.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Comapnys
{
    /// <summary>
    /// 服务商
    /// </summary>
    public class SubordinateCompanyMap : EFEntityTypeConfiguration<SubordinateCompany>
    {
        public SubordinateCompanyMap()
        {
            this.HasKey(t => t.Id);
            this.HasRequired(t => t.Comapny)
                      .WithMany()
                      .HasForeignKey(t => t.CompanyId);
        }
    }
}
