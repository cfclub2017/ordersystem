using Domain.Model.Tasks;

namespace EF.Data.Mapping.Tasks
{
    public partial class ScheduleTaskMap : EFEntityTypeConfiguration<ScheduleTask>
    {
        public ScheduleTaskMap()
        {
            this.ToTable("ScheduleTask");
            this.HasKey(t => t.Id);
            this.Property(t => t.Name).IsRequired();
            this.Property(t => t.Type).IsRequired();
        }
    }
}