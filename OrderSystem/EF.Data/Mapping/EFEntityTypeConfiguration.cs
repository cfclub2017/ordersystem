using Domain.Model.Users;
using System;
using System.Data.Entity.ModelConfiguration;

namespace EF.Data.Mapping
{
    public abstract class EFEntityTypeConfiguration<T> : EntityTypeConfiguration<T> where T : class
    {
        protected EFEntityTypeConfiguration()
        {
            PostInitialize();
        }

        /// <summary>
        /// Developers can override this method in custom partial classes
        /// in order to add some custom initialization code to constructors
        /// </summary>
        protected virtual void PostInitialize()
        {
           
        }
    }
}