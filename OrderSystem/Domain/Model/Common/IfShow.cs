﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Common
{
    public enum IfShow
    {
        [SelectDisplayName("显示")]
        Pending = 1,
        [SelectDisplayName("隐藏")]
        Submit = 2

    }
}
