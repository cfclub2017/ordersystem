﻿using Common.Enums;


namespace Domain.Model.Common
{
    public enum Level
    {
        [SelectDisplayName("一级")]
        One = 1,
        [SelectDisplayName("二级")]
        Two = 2
    }
}
