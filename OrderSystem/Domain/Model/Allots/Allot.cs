﻿using Domain.Model.Comapnys;
using Domain.Model.Products;
using Domain.Model.Stores;
using Domain.Model.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Allots
{
    public class Allot:BaseEntity
    {
        public int SubordinateCompanyId { get; set; }
        public SubordinateCompany SubordinateCompany { get; set; }

        public string ProductAttribute { get; set; }
        public int FromStoreId { get; set; }
        public Store FromStore { get; set; }
        public int ToStoreId { get; set; }
        public Store ToStore { get; set; }
        public int Status { get; set; }
        public int CreateUserId { get; set; }
        public UserAccount CreateUser { get; set; }
        public DateTime CreateTime { get; set; }
        public string Remark { get; set; }
    }
}
