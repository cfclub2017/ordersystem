﻿using Domain.Model.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Allots
{
    public class AllotItem : BaseEntity
    {
        public int AllotId { get; set; }
        public Allot Allot { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public string Remarks { get; set; }
    }
}
