﻿using Domain.Model.Comapnys;
using Domain.Model.Media;
using Domain.Model.Stores;
using Domain.Model.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Orders
{
    public class Order : BaseEntity
    {
        private ICollection<OrderItem> _orderItems;
        private ICollection<Picture> _orderPictures;

        public string OrderNum { get; set; }
        public int Status { get; set; }

        public string CustomerName { get; set; }

        public string CustomerTel1 { get; set; }

        public string CustomerTel2 { get; set; }

        public string Province { get; set; }

        public string City { get; set; }

        public string County { get; set; }

        public string Address { get; set; }

        public string Remarks { get; set; }

        /// <summary>
        /// 要求送货时间
        /// </summary>
        public DateTime? RequireDeliveryTime { get; set; }
        /// <summary>
        /// 要求安装时间
        /// </summary>
        public DateTime? RequireInstallTime { get; set; }

        /// <summary>
        /// 指派服务商ID
        /// </summary>
        public int? FWSID { get; set; }
        public virtual SubordinateCompany FWS { get; set; }
        /// <summary>
        /// 服务商技师姓名
        /// </summary>
        public string FWSUserName { get; set; }
        /// <summary>
        /// 服务商技师联系方式
        /// </summary>
        public string FWSUserTel { get; set; }
        /// <summary>
        /// 服务商技师完成时间
        /// </summary>
        public DateTime? FWSUserCompleteTime { get; set; }
        public int? DGId { get; set; }
        ///// <summary>
        ///// 导购
        ///// </summary>
        public virtual UserAccount DG { get; set; }
        public int? MDId { get; set; }
        ///// <summary>
        ///// 门店
        ///// </summary>
        public virtual SubordinateCompany MD { get; set; }
        public int? JXSId { get; set; }
        ///// <summary>
        ///// 经销商
        ///// </summary>
        public virtual SubordinateCompany JXS { get; set; }
        public int? AgentId { get; set; }
        ///// <summary>
        ///// 代理
        ///// </summary>
        public virtual SubordinateCompany Agent { get; set; }
        public int? AreaId { get; set; }
        ///// <summary>
        ///// 区域
        ///// </summary>
        public virtual SubordinateCompany Area { get; set; }

        public int CreateUserId { get; set; }
        public virtual UserAccount CreateUser { get; set; }

        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 导购提交时间
        /// </summary>
        public DateTime? DGSubmitTime { get; set; }
        /// <summary>
        /// 经销商审核时间
        /// </summary>
        public DateTime? JSXCheckTime { get; set; }
        /// <summary>
        /// 区域审批时间
        /// </summary>
        public DateTime? AreaCheckTime { get; set; }
        /// <summary>
        /// 物流商审批时间
        /// </summary>
        public DateTime? WLCheckTime { get; set; }
        /// <summary>
        /// 物流商发货时间
        /// </summary>
        public DateTime? WLDeliverTime { get; set; }
        /// <summary>
        /// 订单作废时间
        /// </summary>
        public DateTime? CancelTime { get; set; }
        /// <summary>
        /// 师傅交票时间
        /// </summary>
        public DateTime? FWSUserSubmitTime { get; set; }
        /// <summary>
        ///  师傅交票状态
        /// </summary>
        public int FWSUserSubmitStatus { get; set; }
        /// <summary>
        /// 服务商交票时间
        /// </summary>
        public DateTime? FWSSubmitTime { get; set; }
        /// <summary>
        ///  服务商交票状态
        /// </summary>
        public int FWSSubmitStatus { get; set; }
        public int? StoreId { get; set; }
        /// <summary>
        /// 仓库
        /// </summary>
        public virtual Store Store { get; set; }

        /// <summary>
        /// Gets or sets the collection of ProductPicture
        /// </summary>
        public virtual ICollection<Picture> OrderPictures
        {
            get { return _orderPictures ?? (_orderPictures = new List<Picture>()); }
            protected set { _orderPictures = value; }
        }

        public virtual ICollection<OrderItem> OrderItems
        {
            get { return _orderItems ?? (_orderItems = new List<OrderItem>()); }
            protected set { _orderItems = value; }
        }
    }
}
