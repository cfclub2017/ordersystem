﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Orders
{
    public class OrderItem : BaseEntity
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }

        public decimal Price { get; set; }
        public string OSMNum { get; set; }
        public string DNum { get; set; }
        public string SalesNum { get; set; }
        public string VerifyCode { get; set; }
        public int OrderId { get; set; }
        public virtual Order Order { get; set; }

        public string Remarks { get; set; }
        public string CustomerServiceNum { get; set; }
        public DateTime? CustomerServiceTime { get; set; }
        public virtual Products.Product Product { get; set; }
    }
}
