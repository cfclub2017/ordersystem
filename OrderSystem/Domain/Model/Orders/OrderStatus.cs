﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Orders
{
    public enum OrderStatus
    {
        [SelectDisplayName("待提交")]
        Pending = 0,
        [SelectDisplayName("已提交")]
        Submit = 1,
        [SelectDisplayName("区域待审批")]
        AreaApprovalPending = 2,
        [SelectDisplayName("物流待审批")]
        WLApprovalPending = 3,
        [SelectDisplayName("服务商待受理")]
        FWSApprovalPending = 4,
        [SelectDisplayName("服务商已受理")]
        FWSApproval = 5,
        [SelectDisplayName("已配送")]
        Delivery = 6,
        [SelectDisplayName("审批驳回")]
        Rebut = 10,
        [SelectDisplayName("拒绝受理")]
        Reject = 11,
        [SelectDisplayName("作废")]
        Cancel = 12

    }
}
