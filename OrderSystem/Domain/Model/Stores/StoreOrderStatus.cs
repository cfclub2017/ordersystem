﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Stores
{
    public  enum StoreOrderStatus
    {
        [SelectDisplayName("待提交")]
        Pending = 0,
        [SelectDisplayName("已确认")]
        Complete = 1,
        [SelectDisplayName("待审批")]
        NoCheck = 2,

    }
}
