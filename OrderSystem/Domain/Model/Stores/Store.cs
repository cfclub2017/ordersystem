﻿using Domain.Model.Comapnys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Stores
{
    public class Store:BaseEntity
    {
        public string Name { get; set; }

        public string Attribute { get; set; }

        public bool IsSale { get; set; }

        public int FacilitatorId { get; set; }
        /// <summary>
        /// 服务商
        /// </summary>
        public SubordinateCompany Facilitator { get; set; }
    }
}
