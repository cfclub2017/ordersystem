﻿using Domain.Model.Comapnys;
using Domain.Model.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Stores
{
    public class ProductStore : BaseEntity
    {
        public int SubordinateCompanyId { get; set; }
        public SubordinateCompany SubordinateCompany { get; set; }
        public int StoreId { get; set; }
        public virtual Store Store { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }

        public int Quantity { get; set; }

        public int ActualQuantity { get; set; }
    }
}
