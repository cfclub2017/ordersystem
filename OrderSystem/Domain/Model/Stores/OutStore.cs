﻿using Domain.Model.Comapnys;
using Domain.Model.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Stores
{
    public class OutStore : BaseEntity
    {
        public int SubordinateCompanyId { get; set; }
        public SubordinateCompany SubordinateCompany { get; set; }

        public string ProductAttribute { get; set; }
        public int StoreId { get; set; }
        public int Status { get; set; }
        public int CreateUserId { get; set; }
        public UserAccount CreateUser { get; set; }
        public DateTime CreateTime { get; set; }
        public int ConfirmUserId { get; set; }
        public DateTime OutStoreTime { get; set; }
        public int OutStoreType { get; set; }
        public int OutStoreSourceNum { get; set; }
        public string Remark { get; set; }
    }
}
