﻿using Domain.Model.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Stores
{
    public class InStoreItem : BaseEntity
    {
        public int InStoreId { get; set; }
        public InStore InStore { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public string Remarks { get; set; }
    }
}
