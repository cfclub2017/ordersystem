﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Domain.Model.Comapnys;
using Domain.Model.Users;

namespace Domain.Model.Stores
{
    public class InStore : BaseEntity
    {
        public int SubordinateCompanyId { get; set; }
        public SubordinateCompany SubordinateCompany { get; set; }
        public int Num { get; set; }
        public string ProductAttribute { get; set; }
        public int StoreId { get; set; }
        public int Status { get; set; }
        public int CreateUserId { get; set; }
        public UserAccount CreateUser { get; set; }
        public DateTime CreateTime { get; set; }
        public int ConfirmUserId { get; set; }
        public DateTime InStoreTime { get; set; }
        public string CRMNum { get; set; }
        public int InStoreType { get; set; }
        public int InStoreSourceNum { get; set; }
        public string Remark { get; set; }
    }
}
