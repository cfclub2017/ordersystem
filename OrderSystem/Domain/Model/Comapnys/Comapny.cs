﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Comapnys
{
    public class Comapny : BaseEntity
    {
        public string Name { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
