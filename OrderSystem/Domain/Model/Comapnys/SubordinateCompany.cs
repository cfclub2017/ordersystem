﻿using Domain.Model.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Comapnys
{
    /// <summary>
    /// 服务商
    /// </summary>
    public class SubordinateCompany :BaseEntity
    {
        public string Name { get; set; }
        public string Num { get; set; }
        public int Status { get; set; }
        public string Contacts { get; set; }
        public string ContactTel1 { get; set; }
        public string ContactTel2 { get; set; }
        public string Province { get; set; }

        public string City { get; set; }

        public string County { get; set; }

        public string Address { get; set; }

        public string Remarks { get; set; }
        public DateTime CreateTime { get; set; }
        public int ParentId { get; set; }
        public int CompanyType { get; set; } 
        public int CompanyId { get; set; }
        public Comapny Comapny { get; set; }

        public ICollection<Store> Stores { get; set; }
    }
}
