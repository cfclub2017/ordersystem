﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Comapnys
{
    public enum SubordinateComapnyType
    {
        [SelectDisplayName("所有")]
        ALL = -1,
        [SelectDisplayName("服务商")]
        FWS =10,
        [SelectDisplayName("区域")]
        Area =20,
        [SelectDisplayName("代理商")]
        Agent =30,
        [SelectDisplayName("经销商")]
        JXS =40,
        [SelectDisplayName("门店")]
        MD =50,
        [SelectDisplayName("物流商")]
        WL = 60
    }
}
