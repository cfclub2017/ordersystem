﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Users
{
    public enum UserStatus
    {
        [SelectDisplayName("正常")]
        Pending = 1,
        [SelectDisplayName("停用")]
        Submit = 2

    }
}
