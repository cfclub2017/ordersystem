﻿using Domain.Model.Comapnys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Users
{
    public class Role:BaseEntity
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Remarks { get; set; }
        public DateTime CreateTime { get; set; }
        public virtual List<RoleMenus> RoleMenusList { get; set; }
    }
}
