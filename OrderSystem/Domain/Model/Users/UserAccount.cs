﻿using Domain.Model.Comapnys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Users
{
    public class UserAccount:BaseEntity
    {
        public string Name { get; set; }
        //public DateTime? Birthday { get; set; }
        //public string Education { get; set; }
        //public bool IsDispalyTel { get; set; }
        public string Tel { get; set; }
        //public string Email { get; set; }
        //public string Age { get; set; }
        public string UserName { get; set; }
        public string UserPwd { get; set; }
        //public DateTime? InCompanyTime { get; set; }
        public int SubordinateCompanyId { get; set; }
        //public int JobId { get; set; }

        public int CompanyID { get; set; }

        public int UserType { get; set; }
        public int UserState { get; set; }
        public DateTime CreateTime { get; set; }

        public int RoleID { get; set; }

        public virtual SubordinateCompany SubordinateCompany { get; set; }
        public virtual Role Role { get; set; }
        //public string UserAvaterUrl { get; set; }
        //public DateTime? Dirthday { get; set; }
        //public int Sex { get; set; }
        //public string Signature { get; set; }
    }
}
