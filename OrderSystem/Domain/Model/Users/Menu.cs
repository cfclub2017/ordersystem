﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Users
{
    public class Menu : BaseEntity
    {
        public string Name { get; set; }
        public int OrderNo { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }
        public int ParentId { get; set; }
        public string IconClass { get; set; }
        public string Remarks { get; set; }
        public int Status { get; set; }
        public int IfShow { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
