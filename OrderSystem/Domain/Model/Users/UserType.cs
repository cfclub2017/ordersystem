﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Users
{
    public enum UserType
    {
        [SelectDisplayName("管理员")]
        Admin =10,
        [SelectDisplayName("导购")]
        DG =20,
        [SelectDisplayName("区域")]
        QY = 30,
        [SelectDisplayName("经销商")]
        JXS = 40,
        [SelectDisplayName("物流商")]
        WLS = 50,
        [SelectDisplayName("服务商")]
        FWS=60
    }
}
