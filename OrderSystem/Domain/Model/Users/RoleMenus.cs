﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Users
{
    public class RoleMenus : BaseEntity
    {
        public int RoleID { get; set; }
        public int MenuID { get; set; }
        public virtual Role Role { get; set; }
        public virtual Menu Menu { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
