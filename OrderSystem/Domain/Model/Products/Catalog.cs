﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Products
{
    public class Catalog:BaseEntity
    {
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string Name { get; set; }
        public string Num { get; set; }
        public string Icon { get; set; }
        public string ImageMimeType { get; set; }
        public int ParentId { get; set; }
        //public virtual Catalog Parent { get; set; }
        public string ParentName{ get; set; }
        public string Remarks { get; set; }
        public int Level { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
