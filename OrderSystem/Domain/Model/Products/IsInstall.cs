﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Products
{
    public enum IsInstall
    {
        [SelectDisplayName("全部")]
        All = -1,
        [SelectDisplayName("是")]
        YES = 1,
        [SelectDisplayName("否")]
        NO = 0
    }
}
