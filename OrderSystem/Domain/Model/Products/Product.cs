﻿using Domain.Model.Stores;
using Domain.Model.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Products
{
    public class Product:BaseEntity
    {
        private ICollection<ProductStore> _productStores;
        public string Num { get; set; }
        public string Name { get; set; }

        //型号
        public string Specification { get; set; }

        public string Remarks { get; set; }

        public byte[] Img { get; set; }

        public string Cover { get; set; }

        public decimal SuggestPrice { get; set; }

        public decimal MaxPrice { get; set; }

        public decimal MinPrice { get; set; }

        public decimal CostPrice { get; set; }

        public decimal CRMNum { get; set; }

        public int Status { get; set; }

        public bool IsInstall { get; set; }

        public int UserId { get; set; }

        public UserAccount CreateUser { get; set; }

        public DateTime CreateTime { get; set; }
        public int CatalogId { get; set; }
        public int ParentCataloglogId { get; set; }
        public virtual Catalog Catalog { get; set; }
        public virtual Catalog ParentCatalog { get; set; }

        public virtual ICollection<ProductStore> ProductStores
        {
            get { return _productStores ?? (_productStores = new List<ProductStore>()); }
            protected set { _productStores = value; }
        }
    }
}
