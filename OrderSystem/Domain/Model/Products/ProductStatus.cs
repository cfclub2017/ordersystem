﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model.Products
{
    public enum ProductStatus
    {
        [SelectDisplayName("全部")]
        ALL = -1,
        [SelectDisplayName("待审核")]
        DSH = 1,
        [SelectDisplayName("上架")]
        SJ = 2,
        [SelectDisplayName("下架")]
        XJ = 3
    }
}
